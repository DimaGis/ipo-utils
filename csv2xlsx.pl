#!/usr/bin/perl

#Конвертор csv в xlsx
use strict;
use Text::CSV;
use Excel::Writer::XLSX;
use Data::Printer;
use Getopt::Long;
use File::Basename;
use utf8;

our $VERSION = 0.2;

GetOptions(
	'out=s'	=> \my $output
);

$| = 1;
my @ws;
my $csv = Text::CSV->new({eol => "\n", binary => 1, always_quote => 1});
$output = 'Result' unless ($output);
die "File $output.xlsx exists" if (-e $output.'xlsx');
my ($workbook, $rows_format) = new_xlsx($output);
my $strout = "File $output";
if (!@ARGV) {
	die "Usage: $0 file.csv ... [--out filename]\n";
}
for my $fname (@ARGV) {
	my ($wname) = fileparse($fname);
	($wname) = $wname =~ /(.*)?\./;
	utf8::decode($wname);
	open(my $fh, '<:utf8', $fname) or die "Can't open file $fname, $!";
	my $row_header = $csv->getline($fh);
	my $worksheet = add_worksheet($workbook, $wname, $row_header);
	my $row_c = 1;
	my $files_counter = 1;
	while (my $row = $csv->getline($fh)) {
		my $i = 0;
		for (@$row) {
			$worksheet->write_string($row_c, $i++ , $_, $rows_format);
		}
		++$row_c;
		printf("\r%s Rows: %d", $strout, $row_c);
		if ($row_c == 1000000) {
			$workbook->close();
			die "Sorry: 1M+ rows is not supported yet.";
			($workbook, $rows_format) = new_xlsx("Result.$files_counter");
			$worksheet = add_worksheet($workbook, $wname, $row_header);
			print "\n";
			$strout = "File Result.$files_counter.xlsx\n";
			++$files_counter;
			$row_c = 1;
		}
	}
	close($fh);
	print "\n";
}



sub new_xlsx {
	my $fname = shift;
	my $workbook = Excel::Writer::XLSX->new("$fname.xlsx");
	my $header_format = $workbook->add_format(font => 'Arial', size => 10, locked => 1, bold => 1);
	my $rows_format = $workbook->add_format(font => 'Arial', size => 10);
	return ($workbook, $rows_format);
}

sub add_worksheet {
	my ($workbook, $name, $header) = @_;
	my $header_format = $workbook->add_format(font => 'Arial', size => 10, locked => 1, bold => 1);
	my $rows_format = $workbook->add_format(font => 'Arial', size => 10);
	my $worksheet = $workbook->add_worksheet($name);
	$worksheet->set_column(0, 10, 30, 100);
	$worksheet->freeze_panes(1, 0);
	my $i = 0;
	for (@$header) {
		$worksheet->write_string(0, $i++, $_, $header_format);
	}	
	return $worksheet;
}


