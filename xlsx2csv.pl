#!/usr/bin/perl

use strict;
use warnings;
use Text::CSV;
use IPO::Reader::XLSX;

$| = 1;
die "File Result.csv exists" if (-f 'Result.csv');
my $fname	= shift or die "Usage: $0 input.xlsx\n";
my $parser	= IPO::Reader::XLSX->new($fname);
my $csv		= Text::CSV->new({always_quote => 1, binary => 1, eol => "\n"});
open(my $fh, ">:utf8", "Result.csv") or die "Can't open file Result.csv, $!"; 
my $i = 0;
my $row_max = $parser->max_row;
$parser->read(sub {
	my $row = shift;
	$csv->print($fh, $row);
	print "\r$i/$row_max";
	++$i;
});
close($fh);
print "\n";
