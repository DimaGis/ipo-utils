use utf8;
use Mojolicious::Lite;
use Mojo::Home;
use Mojo::Util ('trim', 'md5_sum');
use Mojo::IOLoop::Stream;
use Mojo::EventEmitter;
use Parse::JCONF;
use JCONF::Writer;
use File::Temp 'tmpnam';
use Spreadsheet::XLSX;
use Text::CSV;
use Fcntl ':flock';
use DBI;
use IPOd::Client::Mail;
use POSIX ':signal_h';
use constant CONFIG => Mojo::Home->new->detect(__PACKAGE__) . '/phones-processor.cfg';

my $cfg = Parse::JCONF->new(autodie => 1)->parse_file(CONFIG);
my $progress;
my $progress_emitter = Mojo::EventEmitter->new;

post '/login' => sub {
	my $self = shift;
	
	if ($self->param('login') eq $cfg->{web_auth}{login} && $self->param('passw') eq $cfg->{web_auth}{passw}) {
		$self->signed_cookie('login' => $self->param('login'),  {expires => time+360*24*60*60});
		$self->signed_cookie('passw' => md5_sum($self->param('passw')),  {expires => time+360*24*60*60});
		
		$self->redirect_to('/');
		return 1;
	}
	
	$self->render('auth', error => 1);
};

under sub {
	my $self = shift;
	
	if ($self->req->headers->user_agent =~ /\bmsie\b/i) {
		# we hate IE
		return $self->redirect_to('http://bit.ly/u1Er3y');
	}
	
	unless ($self->signed_cookie('login') eq $cfg->{web_auth}{login} && $self->signed_cookie('passw') eq md5_sum($cfg->{web_auth}{passw})) {
		$self->render('auth');
		return;
	}
	
	1;
};

post '/logout' => sub {
	my $self = shift;
	
	$self->signed_cookie('login' => '',  {expires => time-360*24*60*60});
	$self->signed_cookie('passw' => '',  {expires => time-360*24*60*60});
	
	$self->redirect_to('/');
};

get '/' => sub {
	my $self = shift;
	$self->render('adm', emails => $cfg->{mailto}, progress => $progress);
};

post '/upload' => sub {
	my $self = shift;
	$self->inactivity_timeout(600);
	
	my $phones_file = $self->param('phones-file');
	my $tmp_xlsx = tmpnam();
	$phones_file->move_to($tmp_xlsx);
	
	my $excel = Spreadsheet::XLSX->new($tmp_xlsx);
	my $sheet = $excel->{Worksheet}[0];
	$sheet->{MaxCol} ||= $sheet->{MinCol};
	my @header;
	
	for my $col ($sheet->{MinCol}..$sheet->{MaxCol}) {
		utf8::decode($sheet->{Cells}[0][$col]{Val});
		push @header, $sheet->{Cells}[0][$col]{Val};
	}
	
	unlink $tmp_xlsx;
	$self->render(json => \@header);
};


post '/start' => sub {
	my $self = shift;
	$self->inactivity_timeout(600);
	
	return $self->redirect_to('/') if $progress;
	
	my $phones_file = $self->param('phones-file');
	my $tmp_xlsx = tmpnam();
	$phones_file->move_to($tmp_xlsx);
	
	my $excel = Spreadsheet::XLSX->new($tmp_xlsx);
	my $sheet = $excel->{Worksheet}[0];
	$sheet->{MaxRow} ||= $sheet->{MinRow};
	$sheet->{MaxCol} ||= $sheet->{MinCol};
	
	my @mailto = map { trim $_ } split /,/, $self->param('mailto');
	$cfg->{mailto} = \@mailto;
	my $jconf = JCONF::Writer->new(autodie => 1)->from_hashref($cfg);
	open my $fh, '>:utf8', CONFIG or die 'open: ', $!;
	flock $fh, LOCK_EX or die 'flock: ', $!;
	print $fh $jconf or die 'print: ', $!;
	close $fh or die 'close: ', $!;
	
	my $dbh = DBI->connect(
		"dbi:mysql:database=$cfg->{db}{name};host=$cfg->{db}{host};mysql_connect_timeout=10",
		$cfg->{db}{user},
		$cfg->{db}{pass},
		{RaiseError => 1, mysql_enable_utf8 => 1, PrintError => 0}
	);
	my $sth = $dbh->prepare(
		'SELECT ir_id, hidden, archived FROM card_phone_contacts
		 LEFT JOIN phone_zone on phone_zone.id=phone_zone_id
		 WHERE card_id=? AND CONCAT(IFNULL(phone_zone.value, ""), card_phone_contacts.value)=?'
	);
	
	my $ci_col = $self->param('card_id');
	my $pwc_col = $self->param('phone_with_code');
	
	my $tmp_csv = tmpnam();
	open $fh, '>:utf8', $tmp_csv or die 'open: ', $!;
	my $csv = Text::CSV->new({eol => "\n", binary => 1});
	$csv->print($fh, [qw/CardCode Code OperationType/]);
	
	eval {
		for my $r (1..$sheet->{MaxRow}) {
			for ($sheet->{Cells}[$r][$ci_col]{Val}, $sheet->{Cells}[$r][$pwc_col]{Val}) {
				/^\d+$/ or die 'строка '.($r+1)." содержит нечисловые данные\n";
			}
			
			$sth->execute($sheet->{Cells}[$r][$ci_col]{Val}, $sheet->{Cells}[$r][$pwc_col]{Val});
			my $cnt = 0;
			while (my ($ir_id, $hidden, $archived) = $sth->fetchrow_array()) {
				$cnt++;
				next if !$ir_id || $hidden || $archived;
				$csv->print($fh, [$sheet->{Cells}[$r][$ci_col]{Val}, $ir_id, 'Processing']);
			}
			$cnt or die sprintf("Контакт %s в карточке %s не существует\n", $sheet->{Cells}[$r][$pwc_col]{Val}, $sheet->{Cells}[$r][$ci_col]{Val});
		}
		
		close $fh or die 'close: ', $!;
		
		my $tmp_csv_out = tmpnam();
		open $fh, '-|', 'perl', $cfg->{ir_contacts}{path}, '--process', '--modified-by', $cfg->{ir_contacts}{guid}, '--input', $tmp_csv, '--output', $tmp_csv_out
			or die "Невозможно запустить вспомогательный процесс: $!\n";
		
		$progress = ' ';
		
		my $stream = Mojo::IOLoop::Stream->new($fh);
		my $rm_stream = sub {
			$stream->unsubscribe('close')
                   ->unsubscribe('timeout')
                   ->unsubscribe('error')
                   ->unsubscribe('read');
            
            # AHTUNG: SIGCHLD may break select() inside IO::Socket::INET and connection will fail
            # so, temporarily block it
			my $sigset = POSIX::SigSet->new(SIGCHLD);
			my $old_sigset = POSIX::SigSet->new;
			defined sigprocmask(SIG_BLOCK, $sigset, $old_sigset)
				or warn "Can't block SIGCHLD: ", $!;
			
			my $mailer = IPOd::Client::Mail->new(
				$cfg->{ipod}{host},
				$cfg->{ipod}{port},
				$cfg->{ipod}{user},
				$cfg->{ipod}{pass}
			);
			$mailer->send(
				$cfg->{mailto},
				'Contacts processed at '.localtime,
				$progress
			);
			
			sigprocmask(SIG_UNBLOCK, $old_sigset);
			
			$progress = 0;
			$progress_emitter->emit('progress');
			unlink $tmp_csv;
			unlink $tmp_csv_out;
			undef $stream;
		};
		$stream->timeout(60);
		$stream->on(read => sub {
			my $bytes = trim(pop);
			$bytes =~ s/.*\r//;
			if ($bytes) {
				$progress = $bytes;
				$progress_emitter->emit('progress');
			}
		});
		$stream->on(timeout => sub {
			$rm_stream->();
		});
		$stream->on(close => sub {
			$rm_stream->();
		});
		$stream->on(error => sub {
			$rm_stream->();
		});
		
		$stream->start();
	};
	if (my $e = $@) {
		$self->flash(error => $e);
	}
	
	unlink $tmp_xlsx;
	$self->redirect_to('/');
};

websocket '/progress' => sub {
	my $self = shift;
	$self->inactivity_timeout(300);
	
	my $cb = sub {
		$self->send($progress);
	};
	$progress_emitter->on(progress => $cb);
	
	$self->on(finish => sub {
		$progress_emitter->unsubscribe(progress => $cb);
	});
};

app->start;

__DATA__
@@ layouts/root.html.ep
<!DOCTYPE html>
<html lang="ru">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta http-equiv="Pragma" content="no-cache" />
		<title><%= title %></title>
		%= javascript 'https://code.jquery.com/jquery-1.11.1.min.js';
		%= stylesheet '/style.css';
		%= stylesheet '//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css';
	</head>
	<body>
		%= content
	</body>
</html>

@@ auth.html.ep
% layout 'root', title => 'Процессинг телефонов / Авторизация';
<div class="container" id="login-container">
	<div style="position:absolute; top:40%">
		<div class="content">
			<form method='post' action='/login'>
				<fieldset style="text-align:center" class="control-group">
					<div><input type="text" name="login" placeholder="Логин" class="input-centered"></div>
					<div><input type="password" name="passw" placeholder="Пароль" class="input-centered"></div>
					<button class="btn btn-primary" type="submit">Войти</button>
				</fieldset>
			</form>
		</div>
		
		% if (stash 'error') {
			<div class="error">неверный логин или пароль</div>
		% }
	</div>
</div>

@@ adm.html.ep
% layout 'root', title => 'Процессинг телефонов / Управление';
<script>
	$(function() {
		$("#phones-file").change(function(event) {
			
			var doc_cursor	= document.body.style.cursor;
			var this_cursor = this.style.cursor;
			var that = this;
			document.body.style.cursor = this.style.cursor = 'wait';
			
			function cb() {
				document.body.style.cursor = doc_cursor
				that.style.cursor = this_cursor
			}
			
			var data = new FormData();
			$.each(event.target.files, function(key, value) {
				data.append("phones-file", value);
			})
			
			var ci_select = $('#card_id');
			var pwc_select = $('#phone_with_code');
			var option;
			
			$.ajax({
				url: '/upload',
				type: 'POST',
				data: data,
				cache: false,
				dataType: 'json',
				processData: false,
				contentType: false,
				success: function(data, textStatus, jqXHR) {
					for (var i=0; i<data.length; i++) {
						option = $('<option>');
						option.val(i);
						option.text(data[i]);
						
						ci_select.append(option)
						pwc_select.append(option.clone());
					}
					
					$("#step1").addClass('disabled');
					$("#phones-file").prop("disabled", true);
					
					$("#step2").removeClass('disabled');
					ci_select.prop("disabled", false);
					pwc_select.prop("disabled", false);
					
					cb()
				},
				error: function(jqXHR, textStatus, errorThrown) {
					alert("Ошибка: " + textStatus);
					cb()
				}
			})
		})
		
		$('#card_id, #phone_with_code').change(function() {
			var ci_val = $('#card_id > option:selected').val();
			var pwc_val = $('#phone_with_code > option:selected').val();
			
			if (ci_val != "" && pwc_val != "") {
				$('#step3').removeClass('disabled');
				$('#mailto').prop("disabled", false);
				
				$('#step4').removeClass('disabled');
				$("#start").prop("disabled", false);
			}
		})
		
		$('#start-form').submit(function() {
			$('#start').prop('disabled', true);
			document.body.style.cursor = this.style.cursor = 'wait';
			$('#phones-file').prop('disabled', false);
			return true;
		})
	})
</script>
<div class="container">
	<form method='POST' action='/logout' style="text-align: right">
		<button type="submit" class="btn btn-danger">Выход</button>
	</form>
	
	% if ($progress) {
		<h3>Процесс запущен...</h3>
		<div id="progress"><%= $progress %></div>
		<script>
			var socket = new WebSocket("ws://"+window.location.host+"/progress");
			socket.onmessage = function (event) {
				var progress = event.data;
				if (progress == "0") {
					window.location.href = "/";
				}
				else {
					$('#progress').text(progress)
				}
			}
		</script>
	% }
	% else {
		% if (my $err = flash 'error') {
			% $err =~ s/\n/\\n/g;
			<script>alert("Ошибка: <%= $err %>")</script>
		% }
		<form method='POST' action='/start' enctype="multipart/form-data" id="start-form">
			<div class="step" id="step1">
				<h3>1. Выберите файл с телефонами (xlsx)</h3>
				<input type="file" name="phones-file" id="phones-file">
			</div>
			
			<div class="step disabled" id="step2">
				<h3>2. Сопоставьте колонки</h3>
				<table class="table">
					<tr>
						<th>ID карточки</th><th>Номер с кодом</th>
					</tr>
					<tr>
						<td>
							<select name="card_id" id="card_id" disabled>
								<option value=""></option>
							</select>
						</td>
						<td>
							<select name="phone_with_code" id="phone_with_code" disabled>
								<option value=""></option>
							</select>
						</td>
					</tr>
				</table>
			</div>
			
			<div class="step disabled" id="step3">
				<h3>3. Укажите e-mail адреса для оповещения (через запятую)</h3>
				<input type="text" size="50" name="mailto" id="mailto" value="<%= eval { join ',', @$emails } %>" disabled>
			</div>
			
			<div class="step disabled" id="step4">
				<h3>4. Запустите</h3>
				<button type="submit" class="btn btn-default" name="start" id="start" disabled>Старт</button>
			</div>
		</form>
	% }
</div>

@@ style.css
#login-container.container {
	width: 300px;
}

.container .content {
	padding: 20px 20px 5px 20px;
	-webkit-border-radius: 10px 10px 10px 10px;
	-moz-border-radius: 10px 10px 10px 10px;
	border-radius: 10px 10px 10px 10px;
	-webkit-box-shadow: 0 1px 2px rgba(0,0,0,.15);
	-moz-box-shadow: 0 1px 2px rgba(0,0,0,.15);
	box-shadow: 0 1px 2px rgba(0,0,0,.15);
}

.input-centered {
	text-align:center;
}

.input-centered::-webkit-input-placeholder {
	text-align:center;
}

.error {
	text-align: center;
	color: red;
}

.step {
	margin-bottom: 50px;
}

table.table {
	width: auto;
}

.disabled {
	color: gray;
}
