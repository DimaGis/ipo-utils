use strict;
use Filesys::SmbClient;
use Net::SMTP;

use constant {
	SMB_USERNAME => '2gis\ipouser',
	SMB_PASSWORD => '2Ub$IpoU$er',
	STAMP_PATH   => 'smb://uk-ftp.2gis.local/IPOsync/stamp.txt',
};

eval {
	my $smb = Filesys::SmbClient->new(
		username  => SMB_USERNAME,
		password  => SMB_PASSWORD,
	);

	my $fh = $smb->open(STAMP_PATH, '0666') or die $!;
	my $stamp = $smb->read($fh, 1024);
	$smb->close($fh);

	if (time() - $stamp > 2*60*60) {
		die "stamp file last changed at ", localtime($stamp);
	}
};
if (my $e = $@) {
	warn $e;
	my @to = ('o.gavrin@2gis.ru', 'd.kozhevnikov@2gis.ru');
	my $from = 'ipo@2gis.ru';
	
	my $smtp = Net::SMTP->new('mx.2gis.local', Timeout => 10);
	$smtp->mail($from);
	$smtp->recipient(@to);
	$smtp->data();
	$smtp->datasend("From: $from\n");
	$smtp->datasend("To: " . join(', ', @to) . "\n");
	$smtp->datasend("Subject: Attrs sync watcher found problem at ".localtime()."\n");
	$smtp->datasend("Content-type: text/plain\n\n");
	$smtp->datasend($e);
	$smtp->quit() or die "SMTP Error: ", $@;
}
