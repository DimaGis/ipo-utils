﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.IO;
using DoubleGis.InfoRussia.Services.Interface.Model;

namespace IRhideCards
{
    class Program
    {
        static void Main(string[] args)
        {
            if (args.Length < 1)
            {
                Console.WriteLine("usage: IRhideCards input");
                return;
            }

            CardServiceClient client = new CardServiceClient();
            StreamReader sr = new StreamReader(args[0]);
            string line;
            Regex digitsRe = new Regex(@"\d+");

            while ((line = sr.ReadLine()) != null) {
                try
                {
                    Match m = digitsRe.Match(line);
                    if (m.Success)
                    {
                        long id = long.Parse(m.Value);
                        Console.WriteLine("Changing status for {0}", id);

                        var card = client.GetXmlCard(id);
                        
                        if (card.Status == CardStatus.Gray)
                        {
                            // already hidden
                            continue;
                        }

                        var cardStCur = new CardStatusDto();
                        cardStCur.CardId = id;
                        cardStCur.Status = card.Status;

                        var cardStNew = client.ChangeStatus(cardStCur, CardStatus.Gray, card.Hidden, null, 25002);
                        if (cardStNew.Status != CardStatus.Gray)
                        {
                            throw new Exception("received hidden status after API call != true");
                        }
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine("Exception: {0}", e.Message);
                    Console.WriteLine("Press [b] to break or any key to continue");
                    if (Console.ReadKey().KeyChar == 'b')
                    {
                        break;
                    }
                }
            }

            Console.WriteLine("Done!");
            client.Close();
        }
    }
}
