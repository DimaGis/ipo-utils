#!/usr/bin/perl

use strict;
use DBI;
use Parse::JCONF;
use Fcntl qw(:flock);
use Time::Piece;
use Data::Printer;
use utf8;

open(my $fh_log, ">>:utf8", '/tmp/foranal.log');
$fh_log->autoflush;

if (!flock (DATA, LOCK_EX|LOCK_NB)) {
	_Error("[Error] The process is already running!");
}
print $fh_log localtime->datetime, " [$$] Start process...\n";
my $cfg = Parse::JCONF->new(autodie => 1)->parse_file('foranal.cfg');

sub _dbinput {
	DBI->connect("dbi:mysql:database=" . $cfg->{db_input}{name} . ";host=" . $cfg->{db_input}{host},
			$cfg->{db_input}{user},
			$cfg->{db_input}{pass},
			{RaiseError => 1, mysql_enable_utf8 => 1, PrintError => 0, mysql_auto_reconnect => 1}
			);
}

sub _dboutput {	
	DBI->connect("dbi:mysql:database=" . $cfg->{db_output}{name} . ";host=" . $cfg->{db_output}{host},
			$cfg->{db_output}{user},
			$cfg->{db_output}{pass},
			{RaiseError => 1, mysql_enable_utf8 => 1, PrintError => 0, mysql_auto_reconnect => 1}
			);
}

my ($dbh_input, $dbh_output) = (_dbinput(), _dboutput());

######
#Query1 array
######

#eval {
#	$dbh_output->do('DROP TABLE IF EXISTS analize');
#} or _Error("[Error] Can't drop table", $@);
eval {
	$dbh_output->do('
		CREATE TABLE IF NOT EXISTS `analize` (
		  `id` int(11)			NOT NULL AUTO_INCREMENT,
		  `Project`				VARCHAR(128) NOT NULL,
		  `Start_db_volume`		INT(11) NOT NULL,
		  `Current_db_volume`	INT(11) NOT NULL,
		  `Increment`			INT(11) NOT NULL,
		  `Increment_percent`	DECIMAL(10,2) NOT NULL,
		  `Compare_by_cycle`	INT(11) NOT NULL,
		  `Balance`				INT(11) NOT NULL,
		  `Fact_percent_actual` DECIMAL(10,2) NOT NULL,
		  `Update`				DATETIME NOT NULL,
		  PRIMARY KEY (`id`)
		) DEFAULT CHARSET=utf8');
} or _Error("[Error] Can't create table.", $@);

print $fh_log localtime->datetime, " [$$] Fetch data...\n";
my $time = localtime;
for my $query (@{$cfg->{query1}}) {
	$dbh_input ||= _dbinput();
	my $result = $dbh_input->selectall_arrayref($query, {Slice => {}});
	print $fh_log localtime->datetime, " [$$] Fill data...\n";
	$dbh_output ||= _dboutput();
	for my $row (@$result) {
		eval {
			$dbh_output->do('INSERT INTO	analize 
											(Project,Start_db_volume,Current_db_volume,Increment,Increment_percent,Compare_by_cycle,Balance,Fact_percent_actual,`Update`) 
											VALUE	(?,?,?,?,?,?,?,?,?)', {}, (
								$row->{Project},
								$row->{Start_db_volume},
								$row->{Current_db_volume},
								$row->{Increment},
								$row->{Increment_percent},
								$row->{Compare_by_cycle},
								$row->{Balance},
								$row->{Fact_percent_actual},
								$time->datetime
							));
		} or _Error("[Error] Insert error.", $@);
	}
}

#######
#Query2. Added at 27.10.2015
#######

#eval {
#	$dbh_output->do('DROP TABLE IF EXISTS analize3');
#} or _Error("[Error] Can't drop table", $@);
$dbh_output ||= _dboutput();
eval {
	$dbh_output->do('
		CREATE TABLE IF NOT EXISTS `analize3` (
			`id` 										INT(11)	NOT NULL AUTO_INCREMENT,
			`Проект`									VARCHAR(128) NOT NULL,
			`Дата`										DATE 	NOT NULL,
			`Всего_скрытых`								INT(11) NOT NULL,
			`Временно_не_функционирует`					INT(11) NOT NULL,
			`Исходящая_региональная_реклама`			INT(11) NOT NULL,
			`Не_дозвонились`							INT(11) NOT NULL,
			`Не_сверили_информацию`						INT(11) NOT NULL,
			`Недостаточно_информации`					INT(11) NOT NULL,
			`Отказ_от_размещения`						INT(11) NOT NULL,
			`Отсутствие_разрешающего_документа`			INT(11) NOT NULL,
			`Строительство_заморожено`					INT(11) NOT NULL,
			`Улица_не_привязана`						INT(11) NOT NULL,
			`Новая`										INT(11) NOT NULL,
			`Скрыта_без_причины`						INT(11) NOT NULL,
			`Всего_архивных`							INT(11) NOT NULL,
			`Дом_сдан_в_эксплуатацию`					INT(11) NOT NULL,
			`Дублирующаяся_карточка`					INT(11) NOT NULL,
			`Не_найдено_на_местности`					INT(11) NOT NULL,
			`Не_подлежит_размещению`					INT(11) NOT NULL,
			`Не_удалось_дозвониться_в_течение_2_циклов`	INT(11) NOT NULL,
			`Организация_закрылась`						INT(11) NOT NULL,
			`Отсутствие_дополнительных_контактов`		INT(11) NOT NULL,
			`Снятие_региональной_рекламы`				INT(11) NOT NULL,
			`Отправлена_в_архив_без_причины`			INT(11) NOT NULL,
		PRIMARY KEY (`id`)
		) DEFAULT CHARSET=utf8');
} or _Error("[Error] Can't create table.", $@);

$dbh_input ||= _dbinput();
print $fh_log localtime->datetime, " [$$] Get data query2...\n";
for my $query (@{$cfg->{query2}}) {
	my $result = $dbh_input->selectall_arrayref($query, {Slice => {}});
	
	print $fh_log localtime->datetime, " [$$] Fill data query2...\n";
	$dbh_output ||= _dboutput();
	for my $row (@$result) {
		my @fields;
		my @values;
		
		for my $key (sort keys %$row) {
			push @fields, $key;
			push @values, $row->{$key};
		}
		eval {
			$dbh_output->do('INSERT INTO analize3 (' . join(",", @fields) . ') VALUE (' . join(',', split('', '?'x($#fields+1))) . ')', {}, @values);
		} or _Error("[Error] Insert error.", $@);
	}
}

#######
#Query3. Added at 17.12.2015. Skipped from 26.02.2016
#######
#eval {
#	$dbh_output->do('DROP TABLE IF EXISTS analize4');
#} or _Error("[Error] Can't drop table", $@);
#$dbh_output ||= _dboutput();
#eval {
#	$dbh_output->do('
#		CREATE TABLE IF NOT EXISTS `analize4` (
#			`id` 															INT(11) NOT NULL AUTO_INCREMENT,
#			`Проект`														VARCHAR(128) NOT NULL,
#			`Дата`															DATE 	NOT NULL,
#			`Количество_действующих_БК`										INT(11) NOT NULL,
#			`Количество_действующих_РК`										INT(11) NOT NULL,
#			`Количество_скрытых_без_архивных`								INT(11) NOT NULL,
#			`Количество_действующих_с_выверкой_на_местности`				INT(11) NOT NULL,
#			`Количество_не_привязанных_БК_с_адресом`						INT(11) NOT NULL,
#			`Количество_не_привязанных_БК_без_адреса`						INT(11) NOT NULL,
#			`Количество_не_привязанных_РК_с_адресом`						INT(11) NOT NULL,
#			`Количество_не_привязанных_РК_без_адреса`						INT(11) NOT NULL,
#			`Количество_действующих_БК_больше_6_мес`						INT(11) NOT NULL,
#			`Количество_действующих_РК_больше_6_мес`						INT(11) NOT NULL,
#			`Количество_действующих_без_выверки_на_местности_больше_6_мес`	INT(11) NOT NULL,
#			`Количество_скрытых_больше_6_мес`								INT(11) NOT NULL,
#			`Количество_вычетов_старше_6_мес`								INT(11) NOT NULL,
#			`Телефоны_факсы`												INT(11) NOT NULL,
#			`Веб_сайты`														INT(11) NOT NULL,
#			`EMail`															INT(11) NOT NULL,
#			`Вконтакте`														INT(11) NOT NULL,
#			`Twitter`														INT(11) NOT NULL,
#			`Instagram`														INT(11) NOT NULL,
#			`Fecebook`														INT(11) NOT NULL,
#			`Скрытые_телефоны_факсы`										INT(11) NOT NULL,
#		PRIMARY KEY (`id`)
#		) DEFAULT CHARSET=utf8');
#} or _Error("[Error] Can't create table.", $@);
#
#$dbh_input ||= _dbinput();
#print $fh_log localtime->datetime, " [$$] Get data query3...\n";
#for my $query (@{$cfg->{query3}}) {
#	my $result = $dbh_input->selectall_arrayref($query, {Slice => {}});
#	$dbh_output ||= _dboutput();
#	print $fh_log localtime->datetime, " [$$] Fill data query3...\n";
#	for my $row (@$result) {
#		my @fields;
#		my @values;
#		
#		for my $key (sort keys %$row) {
#			push @fields, $key;
#			push @values, $row->{$key};
#		}
#		eval {
#			$dbh_output->do('INSERT INTO analize4 (' . join(",", @fields) . ') VALUE (' . join(',', split('', '?'x($#fields+1))) . ')', {}, @values);
#		} or _Error("[Error] Insert error.", $@);
#	}
#}

#######
#Query4. Added at 18.12.2015
#######
#eval {
#	$dbh_output->do('DROP TABLE IF EXISTS subtractions');
#} or _Error("[Error] Can't drop table", $@);
$dbh_output ||= _dboutput();
eval {
	$dbh_output->do('
		CREATE TABLE IF NOT EXISTS `subtractions` (
			`id` 		INT(11) NOT NULL AUTO_INCREMENT,
			`Проект`	VARCHAR(128) NOT NULL,
			`Дата`		DATE	NOT NULL,
			`Вычетов`	INT(11) NOT NULL,
		PRIMARY KEY (`id`)
		) DEFAULT CHARSET=utf8');
} or _Error("[Error] Can't create table.", $@);
$dbh_input ||= _dbinput();
print $fh_log localtime->datetime, " [$$] Get data query4...\n";
for my $query (@{$cfg->{query4}}) {
	my $result = $dbh_input->selectall_arrayref($query, {Slice => {}});
	$dbh_output ||= _dboutput();
	print $fh_log localtime->datetime, " [$$] Fill data query4...\n";
	for my $row (@$result) {
		my @fields;
		my @values;
		
		for my $key (sort keys %$row) {
			push @fields, $key;
			push @values, $row->{$key};
		}
		eval {
			$dbh_output->do('INSERT INTO subtractions (' . join(",", @fields) . ') VALUE (' . join(',', split('', '?'x($#fields+1))) . ')', {}, @values);
		} or _Error("[Error] Insert error.", $@);
	}
}

$dbh_input->disconnect();
$dbh_output->disconnect();
print $fh_log localtime->datetime, " [$$] End process...\n";
close($fh_log);

sub _Error {
	my ($msg, $err) = @_;
	utf8::decode($err) unless utf8::is_utf8($err);
	print $fh_log localtime->datetime, " [$$] $msg $err\n";
	close($fh_log);
	exit(0);
}

__END__
