#!/usr/bin/perl

use strict;
use DBI;
use Parse::JCONF;
use Fcntl qw(:flock);
use Time::Piece;

# карточка, номер телефона(код и номер), в третьем столбце сколько раз этот номер повторяется
#ID Карточки	Проект	Номер (с кодом)	Кол-во дублей
#111	Москва	(495)5555555	2
#111	Москва	(495)5555555	2
#111	Москва	(495)9999999	1
#222	Москва	(495)8888888	10


open(my $fh_log, ">>:utf8", '/tmp/foranal2.log');
$fh_log->autoflush;

if (!flock (DATA, LOCK_EX|LOCK_NB)) {
	_Error("[Error] The process is already running!");
}
print $fh_log localtime->datetime, " [$$] Start process...\n";
my $cfg = Parse::JCONF->new(autodie => 1)->parse_file('foranal2.cfg');

my $dbh_input = DBI->connect("dbi:mysql:database=" . $cfg->{db_input}{name} . ";host=" . $cfg->{db_input}{host},
			$cfg->{db_input}{user},
			$cfg->{db_input}{pass},
			{RaiseError => 1, mysql_enable_utf8 => 1, PrintError => 0}
			);
my $dbh_output = DBI->connect("dbi:mysql:database=" . $cfg->{db_output}{name} . ";host=" . $cfg->{db_output}{host},
			$cfg->{db_output}{user},
			$cfg->{db_output}{pass},
			{RaiseError => 1, mysql_enable_utf8 => 1, PrintError => 0}
			);

eval {
	$dbh_output->do('DROP TABLE IF EXISTS analize2');
} or _Error("[Error] Can't drop table", $@);

eval {
	$dbh_output->do('
		CREATE TABLE IF NOT EXISTS `analize2` (
		  `id` int(11)			NOT NULL AUTO_INCREMENT,
		  `Project`				VARCHAR(128) NOT NULL,
		  `CardId`				VARCHAR(28)  NOT NULL,
		  `Phone`				VARCHAR(28)  NOT NULL,
		  `PhoneType`			VARCHAR(28)  NOT NULL,
		  `Count`				TINYINT(8)   NOT NULL,
		  `Update`				DATETIME NOT NULL,
		  PRIMARY KEY (`id`)
		) DEFAULT CHARSET=utf8');
} or _Error("[Error] Can't create table.", $@);

print $fh_log localtime->datetime, " [$$] Fetch data...\n";
my $time = localtime;

my $result = $dbh_input->selectall_arrayref($cfg->{query}, {Slice => {}});

print $fh_log localtime->datetime, " [$$] Fill data...\n";
my %phones;

for my $row (@$result) {
	if (exists($phones{$row->{Project} . $row->{PhoneZone} . $row->{Phone}})) {
		++$phones{$row->{Project} . $row->{PhoneZone} . $row->{Phone}};
	} else {
		$phones{$row->{Project} . $row->{PhoneZone} . $row->{Phone}} = 1;
	}
}

for my $row (@$result) {
	if ($phones{$row->{Project} . $row->{PhoneZone} . $row->{Phone}} > 7) {
		eval {
			$dbh_output->do('INSERT INTO	analize2
											(Project,CardId,Phone,PhoneType,Count,`Update`) 
											VALUE	(?,?,?,?,?,?)', {}, (
								$row->{Project},
								$row->{CardId},
								'(' . $row->{PhoneZone} . ')' . $row->{Phone},
								$row->{PhoneType},
								$phones{$row->{Project} . $row->{PhoneZone} . $row->{Phone}},
								$time->datetime
							));
		} or _Error("[Error] Insert error.", $@);
	}
}

$dbh_input->disconnect();
$dbh_output->disconnect();
print $fh_log localtime->datetime, " [$$] End process...\n";
close($fh_log);

sub _Error {
	my ($msg, $err) = @_;
	print $fh_log localtime->datetime, " [$$] $msg $err\n";
	close($fh_log);
	exit(0);
}

__END__
