#!/usr/bin/perl

use strict;
use warnings;
use DBI;
use Parse::JCONF;
use Fcntl qw(:flock);
use Time::Piece;
use utf8;

open(my $fh_log, ">>:utf8", '/tmp/foranal3.log');
$fh_log->autoflush;

if (!flock (DATA, LOCK_EX|LOCK_NB)) {
	_Error("[Error] The process is already running!");
}
print $fh_log localtime->datetime, " [$$] Start process...\n";
my $cfg = Parse::JCONF->new(autodie => 1)->parse_file('foranal3.cfg');

sub _dbinput {
	DBI->connect("dbi:mysql:database=" . $cfg->{db_input}{name} . ";host=" . $cfg->{db_input}{host},
			$cfg->{db_input}{user},
			$cfg->{db_input}{pass},
			{RaiseError => 1, mysql_enable_utf8 => 1, PrintError => 0, mysql_auto_reconnect => 1}
			);
}

sub _dboutput {	
	DBI->connect("dbi:mysql:database=" . $cfg->{db_output}{name} . ";host=" . $cfg->{db_output}{host},
			$cfg->{db_output}{user},
			$cfg->{db_output}{pass},
			{RaiseError => 1, mysql_enable_utf8 => 1, PrintError => 0, mysql_auto_reconnect => 1}
			);
}

my ($dbh_input, $dbh_output) = (_dbinput(), _dboutput());
$dbh_output ||= _dboutput();
eval {
	$dbh_output->do('
		CREATE TABLE IF NOT EXISTS `analize5` (
			`id`															  INT(11) NOT NULL AUTO_INCREMENT,
			`Дата`															  DATE NOT NULL,
			`Проект`                                                          VARCHAR(128) NOT NULL,
			`Количество_действующих_БК`                                       INT(11) NOT NULL,
			`Количество_действующих_РК`                                       INT(11) NOT NULL,
			`Количество_скрытых_без_архивных`                                 INT(11) NOT NULL,
			`Количество_действующих_с_выверкой_на_местности`                  INT(11) NOT NULL,
			`Количество_не_привязанных_БК_с_адресом`                          INT(11) NOT NULL,
			`Количество_не_привязанных_БК_без_адреса`                         INT(11) NOT NULL,
			`Количество_не_привязанных_РК_с_адресом`                          INT(11) NOT NULL,
			`Количество_не_привязанных_РК_без_адреса`                         INT(11) NOT NULL,
			`Количество_действующих_БК_больше_6_мес`                          INT(11) NOT NULL,
			`Количество_действующих_РК_больше_6_мес`                          INT(11) NOT NULL,
			`Количество_действующих_без_выверки_на_местности_больше_6_мес`    INT(11) NOT NULL,
			`Количество_скрытых_больше_6_мес`                                 INT(11) NOT NULL,
			`Количество_вычетов_больше_6_мес`                                 INT(11) NOT NULL,
			`Всего_карточек_Аварийные_службы`                                 INT(11) NOT NULL,
			`Есть_ОО_Аварийные_службы`                                        INT(11) NOT NULL,
			`Всего_карточек_Врачебные_амбулатории`                            INT(11) NOT NULL,
			`Есть_ОО_Врачебные_амбулатории`                                   INT(11) NOT NULL,
			`Всего_карточек_Диспетчерские_службы`                             INT(11) NOT NULL,
			`Есть_ОО_Диспетчерские_службы`                                    INT(11) NOT NULL,
			`Всего_карточек_Жилищно_коммунальные_услуги`                      INT(11) NOT NULL,
			`Есть_ОО_Жилищно_коммунальные_услуги`                             INT(11) NOT NULL,
			`Всего_карточек_Избирательные_комиссии`                           INT(11) NOT NULL,
			`Есть_ОО_Избирательные_комиссии`					   			  INT(11) NOT NULL,
			`Всего_карточек_МФЦ`                                              INT(11) NOT NULL,
			`Есть_ОО_МФЦ`                                                     INT(11) NOT NULL,
			`Всего_карточек_Отделения_общей_врачебной_практики`               INT(11) NOT NULL,
			`Есть_ОО_Отделения_общей_врачебной_практики`                      INT(11) NOT NULL,
			`Всего_карточек_Отделения_полиции`                                INT(11) NOT NULL,
			`Есть_ОО_Отделения_полиции`                                       INT(11) NOT NULL,
			`Всего_карточек_Поликлиники`                                      INT(11) NOT NULL,
			`Есть_ОО_Поликлиники`                                             INT(11) NOT NULL,
			`Всего_карточек_Почтовые_отделения`                               INT(11) NOT NULL,
			`Есть_ОО_Почтовые_отделения`                                      INT(11) NOT NULL,
			`Всего_карточек_Суды`                                             INT(11) NOT NULL,
			`Есть_ОО_Суды`                                                    INT(11) NOT NULL,
			`Всего_карточек_Участковые_пункты_полиции`                        INT(11) NOT NULL,
			`Есть_ОО_Участковые_пункты_полиции`                               INT(11) NOT NULL,
			`Всего_карточек_Фельдшерско_акушерские_пункты`                    INT(11) NOT NULL,
			`Есть_ОО_Фельдшерско_акушерские_пункты`                           INT(11) NOT NULL,
			`Телефоны_факсы`                                                  INT(11) NOT NULL,
			`Веб_сайты`                                                       INT(11) NOT NULL,
			`EMail`                                                           INT(11) NOT NULL,
			`Вконтакте`                                                       INT(11) NOT NULL,
			`Twitter`														  INT(11) NOT NULL,
			`Instagram`                                                       INT(11) NOT NULL,
			`Fecebook`                                                        INT(11) NOT NULL,
			`Скрытые_телефоны_факсы`                                          INT(11) NOT NULL,
		PRIMARY KEY (`id`)
		) DEFAULT CHARSET=utf8');
} or _Error("[Error] Can't create table.", $@);

$dbh_input ||= _dbinput();
print $fh_log localtime->datetime, " [$$] Get data query...\n";

my $result = $dbh_input->selectall_arrayref($cfg->{query}, {Slice => {}});
$dbh_output ||= _dboutput();
print $fh_log localtime->datetime, " [$$] Fill data query...\n";
for my $row (@$result) {
	my @fields;
	my @values;
	
	for my $key (sort keys %$row) {
		push @fields, $key;
		push @values, $row->{$key};
	}
	eval {
		$dbh_output->do('INSERT INTO analize5 (' . join(",", @fields) . ') VALUE (' . join(',', split('', '?'x($#fields+1))) . ')', {}, @values);
	} or _Error("[Error] Insert error.", $@);
}

$dbh_input->disconnect();
$dbh_output->disconnect();
print $fh_log localtime->datetime, " [$$] End process...\n";
close($fh_log);

sub _Error {
	my ($msg, $err) = @_;
	utf8::decode($err) unless utf8::is_utf8($err);
	print $fh_log localtime->datetime, " [$$] $msg $err\n";
	close($fh_log);
	exit(0);
}

__END__