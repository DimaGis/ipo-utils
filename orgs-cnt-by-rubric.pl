# Скрипт для получения кол-ва организаций в каждой рубрике по всем городам

use strict;
use utf8::all;
use JSON;
use LWP;
use URI::Escape;

my $ua = LWP::UserAgent->new;
print "Enter rubrics, one per line (Ctrl+D to finish):\n";
my @rubrics;

while (<STDIN>) {
	s/\s+$//;
	push @rubrics, $_;
}

my @citylist = map { $_->{name} } @{decode_json($ua->get('http://catalog.api.2gis.ru/project/list?version=1.3&key=1')->decoded_content)->{result}};

for my $r (@rubrics) {
	my $total = 0;
	for my $city (@citylist) {
		my $json = decode_json($ua->get('http://catalog.api.2gis.ru/searchinrubric?what='.uri_escape_utf8($r).'&where='.uri_escape_utf8($city).'&page=1&pagesize=30&sort=relevance&version=1.3&key=runzvw5272')->decoded_content);
		$total += $json->{total};
		print "\t$r: $city - ", $json->{total}, "\n";
	}
	
	print "$r: ", $total, "\n";
}
