#!/usr/bin/perl

use strict;
use IPO::IntegrationBus 0.04;
use IPOd::Client::UniqId;
use IPOd::Client::FlowResponses;
use Parse::JCONF;
use Getopt::Long;
use Text::Trim;
use XML::Writer;
use XML::LibXML;
use DBI;
use POSIX;

$| = 1;

GetOptions(
	"input=s"     => \my $input,
	"output=s"    => \my $output,
	"reason=s"    => \my $reason,
	"date=s"      => \my $date,
	"action=s"    => \my $action,
	"skip-hidden" => \my $skip_hidden,
);

$input && $output && ($action eq 'process' ? 1 : $reason) && $date && (my ($day, $month, $year, $hour, $min) = $date =~ /(\d{2})\.(\d{2})\.(\d{4})\s+(\d{2}):(\d{2})/)
	or die "usage: $0 --input filepath --output filepath --reason hidereason --date 'dd.mm.yyyy hh:mm' [--action archive|hide]\n".
	       "usage: $0 --input filepath --output filepath --date 'dd.mm.yyyy hh:mm' [--skip-hidden] --action process\n";
$action = 'archive' unless $action;
$action = ucfirst lc $action;

unless ($action eq 'Archive' || $action eq 'Hide' || $action eq 'Process') {
	die "Unknown action specified: should be `archive' or `hide' or `process'";
}

my $card_status = 'processed';
my $card_hidden = 'true';

if ($action eq 'Archive') {
	$card_status = 'archived';
}
elsif ($action eq 'Process') {
	$card_hidden = 'false';
}

$day > 0 && $day <= 31 or die "Incorrect day specified";
$month > 0 && $month <= 12 or die "Incorrect month specified";
$year > 2000 && $year < 3000 or die "Incorrect year specified";
$hour >= 0 && $hour < 24 or die "Incorrect hour specified";
$min >= 0 && $min < 60 or die "Incorrect minute specified";

my $time = mktime(0, $min, $hour, $day, $month-1, $year-1900);
if ($time <=0 || $time > time()) {
	die "Incorrect date specified";
}
($min, $hour, $day, $month, $year) = (gmtime($time))[1..5];
$month++;
$year += 1900;

my $cfg = Parse::JCONF->new(autodie => 1)->parse_file(__FILE__.'.cfg');

my $bus = IPO::IntegrationBus->new(
	host     => $cfg->{bus}{host},
	port     => $cfg->{bus}{port},
	app_code => $cfg->{bus}{app_code}
);

my $dbh = DBI->connect(
	'dbi:mysql:database='.$cfg->{db}{name}.';host='.$cfg->{db}{host}.';mysql_connect_timeout=10',
	$cfg->{db}{user},
	$cfg->{db}{pass},
	{RaiseError => 1, mysql_enable_utf8 => 1, mysql_auto_reconnect => 1, PrintError => 0}
);

my $ipod = IPOd::Client::UniqId->new(
	$cfg->{ipod}{host},
	$cfg->{ipod}{port},
	$cfg->{ipod}{user},
	$cfg->{ipod}{pass}
);

open my $fh, '<', $input
	or die "open `$input': $!";

my @id;
while (my $str = <$fh>) {
	trim $str;
	
	if ($str !~ /^\d+$/) {
		die "bad id: ", $str;
	}
	
	push @id, $str;
}

close $fh;

my $reason_id;
if ($reason) {
	my $sth = $dbh->prepare('SELECT id FROM reference_item WHERE reference_id="Card'.$action.'Reason" AND hidden=0 AND name=? LIMIT 1');
	$sth->execute($reason);
	($reason_id) = $sth->fetchrow_array()
		or do {
			$sth = $dbh->prepare('SELECT name FROM reference_item WHERE reference_id="Card'.$action.'Reason" AND hidden=0');
			$sth->execute();
			my $possible_reasons;
			while (my ($reason) = $sth->fetchrow_array()) {
				$possible_reasons .= "$reason\n";
			}
			
			utf8::encode($possible_reasons);
			die "unknown reason: ", $reason, "\n",
				"you might have in mind one of:\n",
				$possible_reasons;
		};
}

my %sent;
my $sent_cnt = 0;
my $recv_cnt = 0;

my $check_date = sprintf('%d-%02d-%02dT%02d:%02d:%02d', $year, $month, $day, $hour, $min, 0);
my $sth = $dbh->prepare('SELECT hidden, archived FROM card where id=?');

for my $id (@id) {
	if ($card_status eq 'processed' && $skip_hidden) {
		$sth->execute($id);
		my ($hidden, $archived) = $sth->fetchrow_array();
		if ($hidden || $archived) {
			warn "$id is ".($archived ? "archived" : "hidden");
			next;
		}
	}
	
	my $xml = XML::Writer->new(OUTPUT => 'self', DATA_INDENT => 2, DATA_MODE => 1);
	
	my $code = $ipod->get_next();
	$sent{$code} = $id;
	
	$xml->emptyTag(
		'CardCheckRequest',
		SourceCode   => 'IPO',
		Code         => $code,
		CardCode     => $id,
		CheckedDate  => $check_date,
		CheckingBy   => '0A5597E0-28E4-4F4C-8174-FE5288AE26ED',
		CardStatus   => $card_status,
		CardIsHidden => $card_hidden,
		$card_hidden eq 'true' ? (ReasonForHiddenCode => $reason_id) : ()
	);
	$bus->send($cfg->{bus}{flow}, [$xml->to_string()]);
	
	print "\rSent: ", ++$sent_cnt, "/", scalar(@id);
}
print "\n";

open $fh, '>:utf8', $output
	or die "open `$output': $!";

my $errors_cnt = 0;
my $unknown_msg_cnt = 0;
my $unknown_msg;

$ipod = IPOd::Client::FlowResponses->new(
	$cfg->{ipod}{host},
	$cfg->{ipod}{port},
	$cfg->{ipod}{user},
	$cfg->{ipod}{pass}
);

while ($sent_cnt != $recv_cnt) {
	my $msgs = $ipod->get($cfg->{bus}{flow}, 'CardCheckResponse');
	%$msgs or sleep 5;
	
	while (my ($code, $msg) = each %$msgs) {
		next unless exists $sent{$code};
		
		my $dom = XML::LibXML->load_xml(string => $msg);
		my $root = $dom->documentElement;
		
		if ($root->getAttribute('SourceCode') ne 'IPO') {
			next;
		}
		
		if ($root->getAttribute('Status') ne 'Approved') {
			$errors_cnt++;
			my $errors = $dom->findnodes('/CardCheckResponse/Errors/Error');
			my $errors_msg = join "\t", map { $_->getAttribute('Description') } @$errors;
			print $fh "$sent{$code}\t$errors_msg\n";
		}
		
		print "\rRecv: ", ++$recv_cnt, sprintf("(success - %d; errors - %d)/", $recv_cnt-$errors_cnt, $errors_cnt), $sent_cnt;
		$ipod->commit($code);
	}
}
print "\n";
close $fh;
