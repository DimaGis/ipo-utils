#!/usr/bin/perl
use strict;
use utf8::all;
use warnings;
use LWP::UserAgent;
use HTML::Strip;
use DBI;
use JSON;
use POSIX;
use Data::Printer;
use feature 'say';


my %urls = ( conversations          => 'https://api.intercom.io/conversations?per_page=60',
             single_conversations   => 'https://api.intercom.io/conversations/',
             admins                 => 'https://api.intercom.io/admins' );

my $ua = LWP::UserAgent->new();
$ua->credentials( 'api.intercom.io:443',
                  'intercom.io',
                  'FPHNKEL7',
                  'ro-a8c3061ead5f4f27de32035a16cedcd4e75a0945' );

my $json = JSON->new->allow_nonref;

my $hs = HTML::Strip->new();

my $dbh = DBI->connect('DBI:mysql:chat:uk-ipo-02.2gis.local', 'dsk', '123qwe!', {
    PrintError => 0,
    RaiseError => 1,
    mysql_enable_utf8 => 1,
});

$dbh->do(' SET NAMES utf8mb4 ');

my $sth_conversations = $dbh->prepare(' INSERT INTO conversations  ( id, creation_date, response_date, closing_date, response_time, duration, assignee_type, assignee_id, message_id, message_body, author_type, author_id, author_length, admins_length, has_attachments, is_open, was_read ) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) ');
my $sth_messages = $dbh->prepare(' INSERT INTO messages  ( id, conversation_id, creation_date, response_time, message_type, message_body, author_type, author_id, has_attachments ) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?) ');
my $sth_admins = $dbh->prepare(' INSERT INTO admins  ( id, name, type, email ) VALUES (?, ?, ?, ?) ');



sub get_content($) {
    my $url = shift;
    my ($code, $error_flag, $body, $base_url);
    
    for (1 .. 5) {
        unless ( !$error_flag && $code == 200  ) {
            say $code if ($code);
            my $res = $ua->get( $url, 'Accept' => 'application/json' );
            $code = $res->code;
            $error_flag = $res->is_error;
            $body = $res->decoded_content;
        }
    }
    return $body;
}



sub seconds_to_hms($) {
    my $seconds = shift;
    if ($seconds > 3020399) {
        return '838:59:59';
    }
    
    my $h = int $seconds/3600;
    my $m = int (($seconds-$h*3600)/60);
    my $s = $seconds-$h*3600-$m*60;
    
    for ($h, $m, $s) {
        if ($_ < 10) {
            $_ = '0'.$_;
        }
    }
    
    return "$h:$m:$s";
}


my $url = $urls{conversations};
while ($url) {
    my $res = get_content($url);
    my $json_convs = from_json($res, { utf8  => 1 });
    $url = $json_convs->{pages}->{next};

    for my $i (0 .. $#{$json_convs->{conversations}}) {
        my @row;
        my $conversation_id = $json_convs->{conversations}->[$i]->{id};
        my $body = $json_convs->{conversations}->[$i]->{conversation_message}->{body};
        my $author_id = $json_convs->{conversations}->[$i]->{conversation_message}->{author}->{id};
        if ($body) { $body = $hs->parse($body) }
        ###say "Conversation: $conversation_id";
        
        
        my $conv_parts_url = $urls{conversations}.$conversation_id;
        my $res = get_content($conv_parts_url);
        my $json_conv_parts = from_json($res, { utf8  => 1 });
        
        my ($response_date, $response_time, $cnt_admins_messages);
        my $cnt_author_messages = 1;
        for my $i (0 .. $#{$json_conv_parts->{conversation_parts}->{conversation_parts}}) {
            my @row;
            
            my $creation_date_msg = $json_conv_parts->{conversation_parts}->{conversation_parts}->[$i]->{created_at};
            my $creation_date_msg_prev;
            if ($i == 0) {
                $creation_date_msg_prev = $json_conv_parts->{created_at};
            }
            else {
                $creation_date_msg_prev = $json_conv_parts->{conversation_parts}->{conversation_parts}->[$i-1]->{created_at}
            }
            
            my $response_time_msg = seconds_to_hms($creation_date_msg - $creation_date_msg_prev);
            my $author_type_msg = $json_conv_parts->{conversation_parts}->{conversation_parts}->[$i]->{author}->{type};
            my $author_id_msg = $json_conv_parts->{conversation_parts}->{conversation_parts}->[$i]->{author}->{id};
            my $message_body = $json_conv_parts->{conversation_parts}->{conversation_parts}->[$i]->{body};
            my $message_type = $json_conv_parts->{conversation_parts}->{conversation_parts}->[$i]->{part_type};
            if ($message_body) { $message_body = $hs->parse($message_body) }
            
            push (@row, (
                            $json_conv_parts->{conversation_parts}->{conversation_parts}->[$i]->{id},
                            $conversation_id,
                            strftime( "%Y-%m-%d %H:%M:%S", localtime ($creation_date_msg) ),
                            $response_time_msg,
                            $message_type,
                            $message_body,
                            $author_type_msg,
                            $author_id_msg,
                            int defined $json_conv_parts->{conversation_parts}->{conversation_parts}->[$i]->{attachments}->[0]
                        )
            );
            ###say "Message: $json_conv_parts->{conversation_parts}->{conversation_parts}->[$i]->{id}";
            eval { $sth_messages->execute(@row) };
            
            if ($author_type_msg eq 'admin' && $author_id_msg ne $author_id && $message_type eq 'comment') {
                $response_date = $json_conv_parts->{conversation_parts}->{conversation_parts}->[$i]->{created_at} unless ($response_date);
                $cnt_admins_messages++;
            }
            
            if ($author_id_msg eq $author_id && $message_type eq 'comment') {
                $cnt_author_messages++;
            }
        }
        
        my $creation_date = $json_convs->{conversations}->[$i]->{created_at};
        my $closing_date = $json_convs->{conversations}->[$i]->{updated_at};
        my $duration = seconds_to_hms($closing_date - $creation_date);
        
        if ($response_date) {
            $response_time = seconds_to_hms($response_date - $creation_date);
            $response_date = strftime( "%Y-%m-%d %H:%M:%S", localtime ($response_date) );
        }
        
        
        push (@row, (
                        $conversation_id,
                        strftime( "%Y-%m-%d %H:%M:%S", localtime ($creation_date) ),
                        $response_date,
                        strftime( "%Y-%m-%d %H:%M:%S", localtime ($closing_date) ),
                        $response_time,
                        $duration,
                        $json_convs->{conversations}->[$i]->{assignee}->{type},
                        $json_convs->{conversations}->[$i]->{assignee}->{id},
                        $json_convs->{conversations}->[$i]->{conversation_message}->{id},
                        $body,
                        $json_convs->{conversations}->[$i]->{conversation_message}->{author}->{type},
                        $author_id,
                        $cnt_author_messages,
                        $cnt_admins_messages,
                        int defined $json_convs->{conversations}->[$i]->{conversation_message}->{attachments}->[0],
                        $json_convs->{conversations}->[$i]->{open}+0,
                        $json_convs->{conversations}->[$i]->{read}+0
                    )
        );
        if ($row[0] == '111111857463239') {
            undef $url;
            last;
        }
        
        eval { $sth_conversations->execute(@row) };
    }
}


$url = $urls{admins};
while ($url) {
    my $res = get_content($url);
    my $json_admins = from_json($res, { utf8  => 1 });
    $url = $json_admins->{pages}->{next};
    
    for my $i (0 .. $#{$json_admins->{admins}}) {
        my @row;
        push (@row, (
                        $json_admins->{admins}->[$i]->{id},
                        $json_admins->{admins}->[$i]->{name},
                        $json_admins->{admins}->[$i]->{type},
                        $json_admins->{admins}->[$i]->{email}
                    )
        );
        
        $sth_admins->execute(@row);
    }
}
