#!/usr/bin/perl

use strict;
use LWP::UserAgent;
use Data::Printer;
use Mojolicious::Lite;
use Mojo::IOLoop;
use Text::CSV;
use IPOd::Client::Mail;
use JSON;
use utf8;
use FindBin;
use Parse::JCONF;

my $VERSION = 0.1;

my %GlobalMap;
my $NowKey = POSIX::mktime(0,0,0,(localtime(time))[3,4,5]);
my @day_interval;
my $PrevKey = $NowKey;
my %result;
my %projects;
my %admins;

END {
	_save_state();
};

my $cfg = Parse::JCONF->new(autodie => 1)->parse_file("$FindBin::Bin/chats.cfg");

my $mailer = IPOd::Client::Mail->new(
        $cfg->{ipod}{host},
        $cfg->{ipod}{port},
        $cfg->{ipod}{user},
        $cfg->{ipod}{pass}
);

$| = 1;
my $csv 	= Text::CSV->new({always_quote => 1, binary => 1, eol => "\n"});
my $ua = LWP::UserAgent->new();
$ua->credentials($cfg->{intercom}{host},
                 $cfg->{intercom}{domain},
                 $cfg->{intercom}{user},
                 $cfg->{intercom}{key}); #Warning R/W key!

my $bReporting = 0;
Mojo::IOLoop->recurring(3600 => sub {
	my $loop = shift;
	$NowKey = POSIX::mktime(0,0,0,(localtime(time))[3,4,5]);
	my ($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst) = localtime(time);
	return if ($NowKey == $PrevKey || $bReporting);
	if ($wday == 0 || $wday == 1) { #Skip Sunday and Monday
		print "Build report Skipped by day: $wday\n";
		$PrevKey = $NowKey;
		%GlobalMap = ();
		return;
	}
	$bReporting = 1;
	print STDERR "Build report\n";
	%result		= ();
	%projects	= ();
	%admins		= ();
	@day_interval = (POSIX::mktime(0,0,0,(localtime(time-3600*24))[3,4,5]), POSIX::mktime(59,59,23,(localtime(time-3600*24))[3,4,5]));
  
	my $resp = $ua->get('https://api.intercom.io/admins', 'Accept' => 'application/json');
	my $regions_get = $ua->get('http://catalog.api.2gis.ru/2.0/region/list?key=runzvw5272&fields=items.code');
	my $json;
	
	eval {
		$json = decode_json($resp->decoded_content);
	} or do { p $resp->decoded_content; die "Can't get admins" };
	
	for (@{$json->{admins}}) {
		#if ($_->{type} eq 'admin') {
			$admins{$_->{id}} = $_->{name};
		#}
	}
	eval {
		$json = decode_json($regions_get->decoded_content);
	} or do { p $regions_get->decoded_content; die "Can't get regions" };
	
	my %regions;
	for (@{$json->{result}{items}}) {
			$regions{$_->{code}} = $_->{name};
	}
	for my $conv_id (keys %{$GlobalMap{$PrevKey}}) {
		my @res = _fetch('https://api.intercom.io/conversations/' . $conv_id);
		_stat(\@res);
	}
	
	open(my $fh1, ">:utf8", "/tmp/Операторы-$PrevKey.csv") or die "Can't open Операторы-$PrevKey.csv for writing, $!";
	open(my $fhp1, ">:utf8", "/tmp/Проекты-$PrevKey.csv") or die "Can't open Проекты-$PrevKey.csv for writing, $!";

	#$csv->print($fh1, ['Оператор', 'Всего диалогов поступило за день', 'Закрытых диалогов','Всего сообщений (он написал)',
	#				  'Среднее время отклика', 'Среднее время отклика на сообщения', 'Количество переоткрытых диалогов',
	#				  'Диалогов привело к зацепкам']);
	$csv->print($fh1, ['Оператор', 'Закрытых диалогов','Всего сообщений (он написал)',
					  'Среднее время отклика', 'Среднее время отклика на сообщения', 'Количество переоткрытых диалогов',
					  'Диалогов привело к зацепкам']);	
	
	for my $admin_id (keys %result) {
		if (exists($result{$admin_id}{comments_firsttime})) {
			my $sum;
			for (@{$result{$admin_id}{comments_firsttime}}) {
				$sum += $_->{admin} - $_->{user};
			}
			$result{$admin_id}{comments_firsttime} =  int($sum / scalar @{$result{$admin_id}{comments_firsttime}});
		}
		if (exists($result{$admin_id}{comments_time})) {
			my $sum;
			for (@{$result{$admin_id}{comments_time}}) {
				$sum += $_->{admin} - $_->{user};
			}
			$result{$admin_id}{comments_time} = int($sum / scalar @{$result{$admin_id}{comments_time}});
		}
		
        if (!exists($admins{$admin_id})) {
            warn "Admins ID not found. ID " . $admin_id;
            warn np $result{$admin_id};
            next;
        }
		#$result{$admin_id}{total_conv} // 0
		$csv->print($fh1, [$admins{$admin_id}, $result{$admin_id}{total_conv_closed} // 0, $result{$admin_id}{comments} // 0,
						  $result{$admin_id}{comments_firsttime} // 0, $result{$admin_id}{comments_time} // 0, $result{$admin_id}{total_conv_reopen} // 0,
						  $result{$admin_id}{total_conv_zacepka} // 0]);
	}
	
	#$csv->print($fhp1, ['Проект', 'Всего диалогов поступило за день', 'Закрытых диалогов']);
	$csv->print($fhp1, ['Проект', 'Закрытых диалогов']);
	
	for my $project (keys %projects) {
		#$csv->print($fhp1, [$project, $projects{$project}{conv} // 0, $projects{$project}{closed} // 0]);
		$csv->print($fhp1, [$regions{$project}, $projects{$project}{closed} // 0]);
	}	
	close($fh1);
	close($fhp1);
	delete($GlobalMap{$PrevKey});
	unlink '/tmp/Result.xlsx' if (-e '/tmp/Result.xlsx');
	eval {
		system('/usr/local/bin/csv2xlsx', "/tmp/Операторы-$PrevKey.csv", "/tmp/Проекты-$PrevKey.csv", "--out", "/tmp/Result");
	} or print STDERR $@;
	#Безденежных Людмила Александровна <l.besdeneschnich@2Gis.ru>
	#Чикалова Алена Сергеевна <a.chikalova@2gis.ru>
	if (-e '/tmp/Result.xlsx') {
		$mailer->send($cfg->{recipients}, 'Daily intercom chat statistics', 'See my attachment', '/tmp/Result.xlsx');
	} else {
		$mailer->send($cfg->{notify}, 'ERROR: Daily intercom chat statistics', "Error: $@");
	}
	$PrevKey = $NowKey;
	$bReporting = 0;
});

if (-e "$FindBin::Bin/saved.state") {
	open(my $fh, '<:utf8', "$FindBin::Bin/saved.state");
	local $/ = undef;
	%GlobalMap = %{from_json(<$fh>)};
	close($fh);
	#unlink('saved.state');
}

#p %GlobalMap;
post '/events' => sub {
	my $self = shift;
#	open(my $fh, ">>:utf8", 'request.json');
#	$fh->autoflush;
	$NowKey = POSIX::mktime(0,0,0,(localtime(time))[3,4,5]);
	if ($self->req->json->{topic} eq 'ping') {
		#print "Pong!\n";
		return $self->render(text => 'pong', status => 200);
	} elsif ($self->req->json->{topic} =~ /^conversation/) {
		my $id = $self->req->json->{data}{item}{id};
		$GlobalMap{$NowKey}{$id} = 1;
		_save_state();
	}
	#print "Topic: ", $self->req->json->{topic}, "\n";
#	print $fh np($self->req->json);
	return $self->render(text => 'ok', status => 200);
#	close($fh);
};

app->start;

sub _save_state {
	open(my $fh, '>:utf8', "$FindBin::Bin/saved.state");
	print $fh to_json(\%GlobalMap);
	close($fh);
	print "State saved.\n";		
}

sub _stat {
	my $conversations = shift;
	for my $item (@$conversations) {
		next if ((!ref($item->{assignee})) || $item->{created_at} > $day_interval[1] || $item->{updated_at} < $day_interval[0]);  #Может быть обновлён уже на след день, нельзя пропускать || $item->{updated_at} > $day_interval[1]);
		if ($item->{created_at} > $day_interval[0] && $item->{created_at} < $day_interval[1]) {
			my $admin_id = $item->{assignee}{id};
			++$result{$admin_id}{total_conv}; #Сколько всего новых диалогов за сегодня
			if ($item->{user}{type} eq 'lead') {
				my $project = getprojectbyid($item->{user}{id});
				++$projects{$project}{conv} if ($project); #Открытых сегодня диалогов в этом проекте
			}
		}
		
		#print "$item->{id}\n";
		my ($firsttime, $reopen, $closed) = (0,0,0);
		my ($parts) = _fetch('https://api.intercom.io/conversations/' . $item->{id});
				#for (@{$parts->{tags}{tags}}) {
				#		if ($_->{id} == 474470) { #ЛК
				#		print 'item: ', $item->{id}, ' parts: ', $parts->{id}, "\n";
				#		exit;
				#	}
				#}
						
		my $user_time = $parts->{created_at};
		my $old_user_time = 0;
		for my $part (@{$parts->{conversation_parts}{conversation_parts}}) {
			my $admin_id;
			if ($part->{author}{type} eq 'admin') {
				$admin_id = $part->{author}{id};
			}
			#next if ($part->{updated_at} < $day_interval[0] || $part->{updated_at} > $day_interval[1] && $part->{author}{type} ne 'user');
			if ($part->{type} eq 'conversation_part' && $part->{part_type} eq 'comment' &&
				$part->{author}{type} eq 'user' && $user_time != $part->{created_at}) {
					$user_time = $part->{created_at};
			} elsif ($part->{updated_at} > $day_interval[0] && $part->{updated_at} < $day_interval[1] &&
					 $part->{type} eq 'conversation_part' && $part->{part_type} eq 'comment' &&
					 $part->{author}{type} eq 'admin') {
						++$result{$admin_id}{comments}; #Всего сообщений он написал
					if (!$firsttime) {
						push @{$result{$admin_id}{comments_firsttime}}, { user => $user_time, admin => $part->{created_at} }; #Набор времён создания первого сообщения в диалоге (новом)
						++$firsttime;
					} elsif ($old_user_time != $user_time) {
						$old_user_time = $user_time;
						push @{$result{$admin_id}{comments_time}}, { user => $user_time, admin => $part->{created_at} }; #Набор времён создания всех сообщений в диалоге
					}
			} elsif ($part->{updated_at} > $day_interval[0] && $part->{updated_at} < $day_interval[1] &&
					 $part->{type} eq 'conversation_part' && $part->{part_type} eq 'open' && !$reopen) {
				++$result{$admin_id}{total_conv_reopen}; #Сколько всего переоткрытых диалогов за сегодня
				++$reopen;
			} elsif ($part->{updated_at} > $day_interval[0] && $part->{updated_at} < $day_interval[1] &&
					 $part->{type} eq 'conversation_part' && $part->{part_type} eq 'close' && !$closed) {
				++$result{$admin_id}{total_conv_closed}; #Закрытых диалогов
				++$closed;
				if (defined($part->{body}) && $part->{body} ne '') {
					++$result{$admin_id}{comments};
				}
				my $project = getprojectbyid($parts->{user}{id});
				++$projects{$project}{closed} if ($project); #Закрытых сегодня диалогов в этом проекте
			} elsif ($part->{updated_at} > $day_interval[0] && $part->{updated_at} < $day_interval[1] &&
					 $part->{type} eq 'conversation_part' && $part->{part_type} eq 'note' && $part->{body} =~ m!http://youla\.2gis\.local/vorwand!) {
				for (@{$parts->{tags}{tags}}) {
					if ($_->{id} == 252291) { #'Создана зацепка'
						++$result{$admin_id}{total_conv_zacepka}; #Диалог привёл к зацепке сeгодня
						last;
					}
				}
			}
		}
	}
}

sub _fetch {
	my $url = shift || die 'Url required.';
	my @ret;
	my $json;
	my $resp = $ua->get($url, 'Accept' => 'application/json');
	eval {
		$json = decode_json($resp->decoded_content);
	} or do { p $resp->decoded_content; die "Can't fetch $url" };
	push @ret, $json;
	if (!exists($json->{pages})) {
		return @ret;
	}
	while ($json->{pages}{next}) {
		$resp = $ua->get($json->{pages}{next}, 'Accept' => 'application/json');
		eval {
			$json = decode_json($resp->decoded_content);
		} or do { p $resp->decoded_content; die "Can't fetch $url" };
		push @ret, $json;
	}
	return @ret;
}

#sub getprojectbyid {
#	my $id = shift || die 'User ID required. ';
#	my ($user) = _fetch('https://api.intercom.io/contacts/' . $id);
#	if ($user) {
#		my ($project) = _fetch("http://catalog.api.2gis.ru/geo/search?types=project&output=json&key=runzvw5272&version=1.3&q=" . $user->{location_data}{longitude} . ',' . $user->{location_data}{latitude});
#		if ($project->{response_code} == 200) {
#			return $project->{result}[0]{name};
#		} else {
#			warn "Can't obtain geo location. ID: $id";
#		}
#	}
#	return;
#}

sub getprojectbyid {
	my $id = shift || die 'User ID required. ';
	my ($user) = _fetch('https://api.intercom.io/events?type=user&intercom_user_id=' . $id);
	if ($user) {
		my ($project) = $user->{events}[0]{metadata}{url} =~ m!2gis\.ru/(.+?)/!i;
		if ($project) {
			return $project;
		} else {
			warn "Can't obtain Project name. ID: $id\nURL: ". $user->{events}[0]{metadata}{url};
		}
	} else {
		warn "Can't obtain Project name. ID: $id";
	}
	return;
}
