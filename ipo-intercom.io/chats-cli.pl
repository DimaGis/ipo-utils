#!/usr/bin/perl

use strict;
use LWP::UserAgent;
use Data::Printer;
use JSON;
use Text::CSV;
use utf8;
use POSIX;
use Getopt::Long;
use Parse::JCONF;
use IPOd::Client::Mail;
use FindBin;

GetOptions(
	'month'	=> \my $bMonth,
	'mail'	=> \my $bMail
);

#Скрипт ручного режима получения данных по дате
#Без параметров, стата за вчера. С указанием диапазона дат типа 1.10.2016 15.10.2016 стата за период
#с ключём --month, запуск возможен только первого числа, за предыдущий месяц.

$| = 1;

my @day_interval;
my @inputData 		= split(/\./, shift);
my @inputDataEnd	= split(/\./, shift);
my $cfg = Parse::JCONF->new(autodie => 1)->parse_file("$FindBin::Bin/chats.cfg");

my $mailer = IPOd::Client::Mail->new(
        $cfg->{ipod}{host},
        $cfg->{ipod}{port},
        $cfg->{ipod}{user},
        $cfg->{ipod}{pass}
);

my ($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst) = localtime(time);

$wday = ($wday == 1) ? 3 : 1;
my $file_operators = "Операторы.csv";
my $file_projects = "Проекты.csv";
if (@inputData) {
	print "Interval\n";
	for my $i (0..2) {
		if ($inputData[$i] =~ /^0/) {
			$inputData[$i] =~ s/^0//;
		}
	}
	--$inputData[1];
	$inputData[2] -= 1900;
	if (@inputDataEnd) {
		for my $i (0..2) {
			if ($inputDataEnd[$i] =~ /^0/) {
				$inputDataEnd[$i] =~ s/^0//;
			}
		}
		--$inputDataEnd[1];
		$inputDataEnd[2] -= 1900;
		@day_interval = (POSIX::mktime(0,0,0,@inputData), POSIX::mktime(59,59,23,@inputDataEnd));
		print "Range created\n";
	} else {
		@day_interval = (POSIX::mktime(0,0,0,@inputData), POSIX::mktime(59,59,23,@inputData));
	}
} elsif ($bMonth) {
	print "Previous month\n";
	die "Today is not a first day of month. It required" if ($mday != 1);
	@day_interval = (POSIX::mktime(0,0,0,1,$mon-1,$year), POSIX::mktime(59,59,23,(localtime(time-3600*24))[3,4,5]));
	($file_operators, $file_projects) = ("/tmp/Операторы.csv", "/tmp/Проекты.csv");
} else {
	print "Previous day\n";
	@day_interval = (POSIX::mktime(0,0,0,(localtime(time-3600*24*$wday))[3,4,5]), POSIX::mktime(59,59,23,(localtime(time-3600*24*$wday))[3,4,5]));
}

#Везде по тексту комментариев под "сегодня" понимается "вчера" ;)
my $csv 	= Text::CSV->new({always_quote => 1, binary => 1, eol => "\n"});
open(my $fh, ">:utf8", $file_operators) or die "Can't open $file_operators for writing, $!";
open(my $fhp, ">:utf8", $file_projects) or die "Can't open $file_projects for writing, $!";
my $ua = LWP::UserAgent->new();
$ua->credentials($cfg->{intercom}{host},
                 $cfg->{intercom}{domain},
                 $cfg->{intercom}{user},
                 $cfg->{intercom}{key}); #Warning R/W key!

my $resp = $ua->get('https://api.intercom.io/admins', 'Accept' => 'application/json');
my $regions_get = $ua->get('http://catalog.api.2gis.ru/2.0/region/list?key=runzvw5272&fields=items.code');
my $json;

eval {
	$json = decode_json($resp->decoded_content);
} or do { p $resp->decoded_content; die "Can't get admins" };

my %admins;
for (@{$json->{admins}}) {
	#if ($_->{type} eq 'admin') {
		$admins{$_->{id}} = $_->{name};
	#}
}
eval {
	$json = decode_json($regions_get->decoded_content);
} or do { p $regions_get->decoded_content; die "Can't get regions" };

my %regions;
for (@{$json->{result}{items}}) {
	$regions{$_->{code}} = $_->{name};
}

my %result;
my %projects;
for (qw/total_conv comments comments_firsttime comments_time total_conv_reopen total_conv_closed total_conv_zacepka/) {}

#for my $admin_id (keys %admins) {
	#$admin_id = 283046; #Мария
#	print "$admin_id\n";
	#my @res = _fetch('https://api.intercom.io/conversations?type=admin&&per_page=50&admin_id=' . $admin_id);
my @res = _fetch('https://api.intercom.io/conversations?per_page=50');
for my $page (@res) {
	_stat($page->{conversations});
}

#$csv->print($fh, ['Оператор', 'Всего диалогов поступило за день', 'Закрытых диалогов','Всего сообщений (он написал)',
#				  'Среднее время отклика', 'Среднее время отклика на сообщения', 'Количество переоткрытых диалогов',
#				  'Диалогов привело к зацепкам']);
$csv->print($fh, ['Оператор', 'Закрытых диалогов','Всего сообщений (он написал)',
				  'Среднее время отклика', 'Среднее время отклика на сообщения', 'Количество переоткрытых диалогов',
				  'Диалогов привело к зацепкам']);

for my $admin_id (keys %result) {
	if (exists($result{$admin_id}{comments_firsttime})) {
		my $sum;
		for (@{$result{$admin_id}{comments_firsttime}}) {
			$sum += $_->{admin} - $_->{user};
		}
		$result{$admin_id}{comments_firsttime} =  int($sum / scalar @{$result{$admin_id}{comments_firsttime}});
	}
	if (exists($result{$admin_id}{comments_time})) {
		my $sum;
		for (@{$result{$admin_id}{comments_time}}) {
			$sum += $_->{admin} - $_->{user};
		}
		$result{$admin_id}{comments_time} = int($sum / scalar @{$result{$admin_id}{comments_time}});
	}
	
    if (!exists($admins{$admin_id})) {
	    warn "Not found admin id " . $admin_id;
        warn np $result{$admin_id};
        next;
    }
    #$csv->print($fh, [$admins{$admin_id}, $result{$admin_id}{total_conv} // 0, $result{$admin_id}{total_conv_closed} // 0, $result{$admin_id}{comments} // 0,
	#				  $result{$admin_id}{comments_firsttime} // 0, $result{$admin_id}{comments_time} // 0, $result{$admin_id}{total_conv_reopen} // 0,
	#				  $result{$admin_id}{total_conv_zacepka} // 0]);
	next if ($admins{$admin_id} eq 'Operators');
	$csv->print($fh, [$admins{$admin_id}, $result{$admin_id}{total_conv_closed} // 0, $result{$admin_id}{comments} // 0,
					  $result{$admin_id}{comments_firsttime} // 0, $result{$admin_id}{comments_time} // 0, $result{$admin_id}{total_conv_reopen} // 0,
					  $result{$admin_id}{total_conv_zacepka} // 0]);	
}

$csv->print($fhp, ['Проект', 'Всего диалогов поступило за день', 'Закрытых диалогов']);

for my $project (keys %projects) {
	if (!exists($regions{$project})) {
		print "Not found key $project\n";
	}
	$csv->print($fhp, [$regions{$project}, $projects{$project}{conv} // 0, $projects{$project}{closed} // 0]);
}

close($fh);
close($fhp);

if ($bMonth) {
	unlink '/tmp/ResultMonth.xlsx' if (-e '/tmp/ResultMonth.xlsx');
	eval {
		system('/usr/local/bin/csv2xlsx', $file_projects, "--out", "/tmp/ResultMonth");
	} or do {
		$mailer->send($cfg->{notify}, 'ERROR: Monthly intercom chat statistics', "Error: $@")
			if ($bMail);
		exit;
	};
	if ($bMail) {
		if (-e '/tmp/ResultMonth.xlsx') {
			$mailer->send($cfg->{recipients}, 'Monthly intercom chat statistics', 'See my attachment', '/tmp/ResultMonth.xlsx');
		} else {
			$mailer->send($cfg->{notify}, 'ERROR: Monthly intercom chat statistics', "Error: $@");
		}		
	}
}


#Всего диалогов поступило за день	Закрытых диалогов	Всего сообщений (он написал)	Среднее время отклика	Среднее время отклика на сообщения	Количество переоткрытых диалогов	Диалогов привело к зацепкам
sub _stat {
	my $conversations = shift;
	for my $item (@$conversations) {
		next if ((!ref($item->{assignee})) || $item->{created_at} > $day_interval[1] || $item->{updated_at} < $day_interval[0]);  #Может быть обновлён уже на след день, нельзя пропускать || $item->{updated_at} > $day_interval[1]);
		if ($item->{created_at} > $day_interval[0] && $item->{created_at} < $day_interval[1]) {
			my $admin_id = $item->{assignee}{id};
			++$result{$admin_id}{total_conv}; #Сколько всего новых диалогов за сегодня
			if ($item->{user}{type} eq 'lead') {
				my $project = getprojectbyid($item->{user}{id});
				++$projects{$project}{conv} if ($project); #Открытых сегодня диалогов в этом проекте
				#print "$project, ", $item->{user}{id}, "\n";
			}
		}
		
		print "$item->{id}\n";
		my ($firsttime, $reopen, $closed) = (0,0,0);
		my ($parts) = _fetch('https://api.intercom.io/conversations/' . $item->{id});
				#for (@{$parts->{tags}{tags}}) {
				#		if ($_->{id} == 474470) { #ЛК
				#		print 'item: ', $item->{id}, ' parts: ', $parts->{id}, "\n";
				#		exit;
				#	}
				#}
						
		my $user_time = $parts->{created_at};
		my $old_user_time = 0;
		for my $part (@{$parts->{conversation_parts}{conversation_parts}}) {
			my $admin_id;
			if ($part->{author}{type} eq 'admin') {
				$admin_id = $part->{author}{id};
			}
			#next if ($part->{updated_at} < $day_interval[0] || $part->{updated_at} > $day_interval[1] && $part->{author}{type} ne 'user');
			if ($part->{type} eq 'conversation_part' && $part->{part_type} eq 'comment' &&
				$part->{author}{type} eq 'user' && $user_time != $part->{created_at}) {
					$user_time = $part->{created_at};
			} elsif ($part->{updated_at} > $day_interval[0] && $part->{updated_at} < $day_interval[1] &&
					 $part->{type} eq 'conversation_part' && $part->{part_type} eq 'comment' &&
					 $part->{author}{type} eq 'admin') {
						++$result{$admin_id}{comments}; #Всего сообщений он написал
					if (!$firsttime) {
						push @{$result{$admin_id}{comments_firsttime}}, { user => $user_time, admin => $part->{created_at} }; #Набор времён создания первого сообщения в диалоге (новом)
						++$firsttime;
					} elsif ($old_user_time != $user_time) {
						$old_user_time = $user_time;
						push @{$result{$admin_id}{comments_time}}, { user => $user_time, admin => $part->{created_at} }; #Набор времён создания всех сообщений в диалоге
					}
			} elsif ($part->{updated_at} > $day_interval[0] && $part->{updated_at} < $day_interval[1] &&
					 $part->{type} eq 'conversation_part' && $part->{part_type} eq 'open' && !$reopen) {
				++$result{$admin_id}{total_conv_reopen}; #Сколько всего переоткрытых диалогов за сегодня
				++$reopen;
			} elsif ($part->{updated_at} > $day_interval[0] && $part->{updated_at} < $day_interval[1] &&
					 $part->{type} eq 'conversation_part' && $part->{part_type} eq 'close' && !$closed) {
				++$result{$admin_id}{total_conv_closed}; #Закрытых диалогов
				++$closed;
				if (defined($part->{body}) && $part->{body} ne '') {
					++$result{$admin_id}{comments};
				}
				my $project = getprojectbyid($parts->{user}{id});
				++$projects{$project}{closed} if ($project); #Закрытых сегодня диалогов в этом проекте
			} elsif ($part->{updated_at} > $day_interval[0] && $part->{updated_at} < $day_interval[1] &&
					 $part->{type} eq 'conversation_part' && $part->{part_type} eq 'note' && $part->{body} =~ m!http://youla\.2gis\.local/vorwand!) {
				for (@{$parts->{tags}{tags}}) {
					if ($_->{id} == 252291) { #'Создана зацепка'
						++$result{$admin_id}{total_conv_zacepka}; #Диалог привёл к зацепке сeгодня
						last;
					}
				}
			}
		}
	}
}

sub _fetch {
	my $url = shift || die 'Url required.';
	my @ret;
	my $json;
	my $resp = $ua->get($url, 'Accept' => 'application/json');
	eval {
		$json = decode_json($resp->decoded_content);
	} or do { p $resp->decoded_content; die "Can't get conversations per admin $url" };
	push @ret, $json;
	if (!exists($json->{pages})) {
		return @ret;
	}
	while ($json->{pages}{next}) {
		$resp = $ua->get($json->{pages}{next}, 'Accept' => 'application/json');
		eval {
			$json = decode_json($resp->decoded_content);
		} or do { p $resp->decoded_content; die "Can't get conversations per admin $url" };
		push @ret, $json;
	}
	return @ret;
}

#sub getprojectbyid {
#	my $id = shift || die 'User ID required. ';
#	my ($user) = _fetch('https://api.intercom.io/contacts/' . $id);
#	if ($user) {
#		my ($project) = _fetch("http://catalog.api.2gis.ru/geo/search?types=project&output=json&key=runzvw5272&version=1.3&q=" . $user->{location_data}{longitude} . ',' . $user->{location_data}{latitude});
#		if ($project->{response_code} == 200) {
#			return $project->{result}[0]{name};
#		} else {
#			warn "Can't obtain geo location. ID: $id";
#		}
#	}
#	return;
#}

sub getprojectbyid {
	my $id = shift || die 'User ID required. ';
	my ($user) = _fetch('https://api.intercom.io/events?type=user&intercom_user_id=' . $id);
	if ($user) {
		my ($project) = $user->{events}[0]{metadata}{url} =~ m!2gis\.ru/(.+?)/!i;
		if ($project) {
			return $project;
		} else {
			warn "Can't obtain Project name. ID: $id\nURL: ". $user->{events}[0]{metadata}{url};
		}
	} else {
		warn "Can't obtain Project name. ID: $id";
	}
	return;
}
