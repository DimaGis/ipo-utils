#!/usr/bin/env perl

use strict;
use warnings;

use v5.10;
use LWP::UserAgent;
use URI::Escape;
use Text::CSV;
use POSIX qw(ceil);
use JSON;
use utf8;

use lib ("/home/un1or/p5/p5-composition/lib");
use composition "LWP::UserAgent" => "LWP::UserAgent::Cached";

use Data::Dumper;

my $ua = LWP::UserAgent::Cached->new(cache_dir => "/home/un1or/cache/old-rubrics");

my $fn_output = shift or die "please specify output!";

print "Enter rubrics, one per line (Ctrl+D to finish):\n";
my @rubrics;

while (<STDIN>) {
	s/\s+$//;
	push @rubrics, $_;
}

my @citylist = map { $_->{name} } @{decode_json($ua->get('http://catalog.api.2gis.ru/project/list?version=1.3&key=1')->decoded_content)->{result}};

open my $output, '>:utf8', $fn_output or die $!;
$output->autoflush(1);
my $csv = Text::CSV->new({sep_char => "\t", binary => 1, eol => "\n"});
$csv->print($output, [qw(rubric uid city title address)]);

binmode STDOUT, ":utf8";

my $per_page = 50;
for my $r (@rubrics) {
    my $rubric = $r;
    utf8::decode($rubric);
    print "- $rubric\n";
	for my $city (@citylist) {
        print "-- $city\n";
        my $url = "http://catalog.api.2gis.ru/searchinrubric?what=".uri_escape($r)."&where=".uri_escape_utf8($city)."&pagesize=${per_page}&sort=relevance&version=1.3&key=runzvw5272";
        my $total_pages = ceil(decode_json($ua->get("${url}&page=1")->decoded_content)->{total} / $per_page);
        for my $p ( 1..$total_pages ) {
            my $json = decode_json($ua->get("${url}&page=${p}")->decoded_content);
            for my $f ( @{$json->{result}} ) {
                my $address = ($f->{city_name} ? "$f->{city_name}, " : "") . ($f->{address} ? $f->{address} : "");
                $csv->print($output, [ $rubric, $f->{id}, $city, $f->{name}, $address ]);
            }
        }
	}
}

