#!/bin/bash

if [ -z "$1" -o -z "$2" ]
then
	echo "usage: $0 input_dir output_dir"
	exit 255
fi

for f in "$1"/*
do
	convert -flatten "$f" "$2"/`basename "$f"` || exit
done
