#!/usr/bin/perl

use strict;
use warnings;
use File::Basename;
use GD;

my ($tpl_dir, $input) = @ARGV;

$tpl_dir && $input
	or die "usage: $0 tpl_dir input";

my %tpl;
my %width;

for my $path (<$tpl_dir/*>) {
	my $img = GD::Image->newFromPng($path, 1);
	my $width = $img->width;
	my $height = $img->height;
	
	my $name = basename($path, '.png');
	$width{$name} = $width;
	
	for my $x (0..$width-1) {
		for my $y (0..$height-1) {
			$tpl{$name}{"${x}x${y}"} = rgb($img, $x, $y);
		}
	}
}

sub rgb {
	my ($img, $x, $y) = @_;
	RGB->new( $img->rgb($img->getPixel($x,$y)) )
}

my $img = GD::Image->newFromPng($input, 1);
my $width = $img->width;
my $height = $img->height;

my $x = 0;

WIDTH_LOOP:
while ($x < $width) {
	keys %width;
	while (my ($name, $need_width) = each %width) {
		if ($need_width > $width - $x) {
			next;
		}
		
		my $found = 1;
		
		TPL_LOOP:
		for my $i (0..$need_width-1) {
			for my $y (0..$height-1) {
				unless (rgb($img, $x+$i, $y) == $tpl{$name}{"${i}x${y}"}) {
					$found = 0;
					last TPL_LOOP;
				}
			}
		}
		
		if ($found) {
			$x += $need_width;
			print $name;
			next WIDTH_LOOP;
		}
	}
	
	$x++;
}

print "\n";

package RGB;

use overload
	'==' => sub {
		my ($self, $other) = @_;
		
		$self->{r} == $other->{r} &&
		$self->{g} == $other->{g} &&
		$self->{b} == $other->{b};
	},
	'""' => sub {
		my $self = shift;
		sprintf('%02x%02x%02x', @$self{qw/r g b/});
	};

sub new {
	my $class = shift;
	
	my %self;
	@self{qw/r g b/} = @_;
	
	bless \%self, $class;
}
