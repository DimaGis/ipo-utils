#!/usr/bin/perl

use strict;
use GD;

my ($input, $pixels, $output) = @ARGV;

$input && $pixels && $output
	or die "usage: $0 input X1xX2 output";

my ($x1, $x2) = split /x/, $pixels;

my $img    = GD::Image->newFromPng($input);
my $height = $img->height;

my $new_img = GD::Image->new($x2-$x1+1, $height);

$new_img->copy($img, 0, 0, $x1, 0, $x2-$x1+1, $height);

open my $fh, '>', $output or die $!;
syswrite($fh, $new_img->png);
close $fh;
