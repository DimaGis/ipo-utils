#!/usr/bin/perl

use strict;
use LWP::UserAgent::Determined;
use LWP::Simple;
use Getopt::Std;

getopt("poe", \my %OPTS);
my $input = shift;
die "usage: $0 -p http_proxy -e img_ext -o output_dir input" unless defined $OPTS{p} && defined $OPTS{o} &&
                                                                    defined $OPTS{e} && defined $input;

$LWP::Simple::ua = LWP::UserAgent::Determined->new(agent => 'Mozilla/5.0', timeout => 10);
$LWP::Simple::ua->proxy(['http', 'https'] => "http://" . $OPTS{p});
$LWP::Simple::ua->timing('1,3,15,30,60');
$LWP::Simple::ua->after_determined_callback(sub {
	my $resp = pop;
	if ($resp->header('X-Died')) {
		$resp->code(500);
	}
});

open FH, $input
	or die "open `$input': $!";

my $i = 1;
while (my $link = <FH>) {
	warn "Fetching $i...\n";
	
	$link =~ s/\s+//g;
	my $fname = sprintf("%s/%04d." . $OPTS{e}, $OPTS{o}, $i++);
	next if -e $fname;
	
	my $resp = getstore($link, $fname);
	unless ($resp =~ /^2/) {
		die "Can't fetch $link: $resp";
	}
}

close FH;
