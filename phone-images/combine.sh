#!/bin/bash

usage() {
	echo "usage: $0 -g group_length -b blank_img -o output_dir input_dir"
	exit 1
}

while getopts "g:b:o:" opt; do
	case $opt in
		g)
			GROUP_LENGTH=$OPTARG;;
		b)
			BLANK_IMG=$OPTARG;;
		o)
			OUTPUT_DIR=$OPTARG;;
		\?)
			usage;;
		:)
			usage;;
	esac
done

shift $((OPTIND-1))
INPUT_DIR="$1"

[ -n "$GROUP_LENGTH" -a -n "$BLANK_IMG" -a -n "$OUTPUT_DIR" -a -n "$INPUT_DIR" ] || usage;

cd "$INPUT_DIR" || exit;

imgs_str=""
i=0
j=1;

for img in *
do
	let i++
	imgs_str+="$img $BLANK_IMG "
	if [ $i -gt $GROUP_LENGTH ]
	then
		echo "converting $i images..."
		convert -append $imgs_str "$OUTPUT_DIR/out.$(printf %03d $j).bmp" || exit
		imgs_str=""
		i=0
		let j++;
	fi
done

if [ -n "$imgs_str" ]
then
	echo "converting $i images..."
	convert -append $imgs_str "$OUTPUT_DIR/out.$(printf %03d $j).bmp"
fi
