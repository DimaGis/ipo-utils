#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    this->setWindowFlags(Qt::WindowStaysOnTopHint);
    mTitle = this->windowTitle();

    connect(QApplication::clipboard(), SIGNAL(dataChanged()), this, SLOT(clipboardChanged()));
    connect(this->ui->webView, SIGNAL(titleChanged(QString)), this, SLOT(webTitleChanged(QString)));
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::clipboardChanged() {
    QString buf = QApplication::clipboard()->text();

    if (buf.indexOf("2gis://") == 0) {
        //QUrl url(QUrl("http://10.54.64.57/diff"));
        QUrl url(QUrl("http://anketa.2gis.ru/diff"));
        QUrlQuery query;
        query.addQueryItem("fq", buf);
        query.addQueryItem("ns", "1");
        url.setQuery(query);

        this->ui->webView->load(url);
        this->ui->webView->setCursor(Qt::BusyCursor);
    }
}

void MainWindow::webTitleChanged(const QString &title) {
    this->setWindowTitle(mTitle + " - " + title);
}
