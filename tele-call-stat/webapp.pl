use utf8;
use Mojolicious::Lite;
use Mojo::Home;
use Mojo::Util 'trim';
use Digest::MD5 'md5_hex';
use List::Util 'first';
use DBI;
use POSIX;

use constant {
	LOGIN       => 'teleroot',
	PASSW       => '12GHnp19',
	SECRET      => ';lkrweopriwpesdfhjkhrweor!',
	PHONE_CFG   => 'phone.cfg',
	MAIL_CFG    => 'mail.cfg',
	HOME        => Mojo::Home->new->detect(),
	DB_NAME     => 'statistic',
	DB_HOST     => '10.54.16.43',
	DB_USER     => 'ipouser',
	DB_PASS     => 'ilove2gisIpoU$er',
	OK_DURATION => $ENV{TELE_OK_DURATION} || 15
};

my $dbh = DBI->connect(
	"dbi:mysql:database=" . DB_NAME . ';host=' . DB_HOST . ';mysql_connect_timeout=10',
	DB_USER,
	DB_PASS,
	{RaiseError => 1, mysql_auto_reconnect => 1, mysql_enable_utf8 => 1}
);

post '/login' => sub {
	my $self = shift;
	
	if ($self->param('login') eq LOGIN && $self->param('passw') eq PASSW) {
		$self->signed_cookie('login' => LOGIN,  {expires => time+360*24*60*60});
		$self->signed_cookie('passw' => md5_hex(PASSW),  {expires => time+360*24*60*60});
		
		$self->redirect_to('/');
		return 1;
	}
	
	$self->render('auth', error => 1);
};

under sub {
	my $self = shift;
	
	unless ($self->signed_cookie('login') eq LOGIN && $self->signed_cookie('passw') eq md5_hex(PASSW)) {
		$self->render('auth');
		return;
	}
	
	1;
};

get '/' => sub {
	my $self = shift;
	$self->render('adm', phones => get_phones(), emails => get_emails(), ok_duration => OK_DURATION);
};

post '/addphone' => sub {
	my $self = shift;
	
	my $phones = get_phones();
	
	my $num      = trim $self->param('num');
	my $operator = trim $self->param('operator');
	
	unless ($num && $operator && $num =~ /^\d+$/) {
		$self->flash(phone_error => 'Некорректные значения');
	}
	elsif (exists $phones->{$num}) {
		$self->flash(phone_error => 'Номер существует');
	}
	else {
		if (open my $fh, '>>:utf8', HOME . '/' . PHONE_CFG) {
			print $fh $self->param('num'), ' = ', $self->param('operator'), "\n";
			close $fh;
		}
		else {
			$self->flash(phone_error => 'Ошибка записи в файл');
		}
	}
	
	$self->redirect_to('/?to=phone');
};

post '/rmphone' => sub {
	my $self = shift;
	
	my $phones = get_phones();
	delete $phones->{$self->param('num')};
	
	if (open my $fh, '>', HOME . '/' . PHONE_CFG) {
		while (my ($num, $operator) = each %$phones) {
			print $fh $num, ' = ', $operator, "\n";
		}
		close $fh;
	}
	else {
		$self->flash(phone_error => 'Ошибка записи в файл');
	}
	
	$self->redirect_to('/?to=phone');
};

post '/addemail' => sub {
	my $self = shift;
	
	my $emails = get_emails();
	my $email = trim $self->param('email');
	
	if (first { $_ eq $email } @$emails) {
		$self->flash(email_error => 'Email существует');
	}
	elsif ($email !~ /^.+?@.+?\..+$/) {
		$self->flash(email_error => 'Некорректное значение');
	}
	else {
		if (open my $fh, '>>:utf8', HOME . '/' . MAIL_CFG) {
			print $fh $email, "\n";
			close $fh;
		}
		else {
			$self->flash(email_error => 'Ошибка записи в файл');
		}
	}
	
	$self->redirect_to('/?to=email');
};

post '/rmemail' => sub {
	my $self = shift;
	
	my $emails = get_emails();
	my $email = trim $self->param('email');
	
	if (open my $fh, '>:utf8', HOME . '/' . MAIL_CFG) {
		for my $m (@$emails) {
			if ($m ne $email) {
				print $fh $m, "\n";
			}
		}
		
		close $fh;
	}
	else {
		$self->flash(email_error => 'Ошибка записи в файл');
	}
	
	$self->redirect_to('/?to=email');
};

get '/stat' => sub {
	my $self = shift;
	my $phones = get_phones();
	
	my $sth = $dbh->prepare(
		"SELECT callingPartyNumber, duration FROM cdr_statistic WHERE dateTimeOrigination between ? AND ? AND originalCalledPartyNumber<>''
		AND substring(originalCalledPartyNumber, 1, 1)<>'b' AND callingPartyNumber IN (".join(',', map { $dbh->quote($_) } keys %$phones).")
		AND duration>=" . OK_DURATION
	);
	
	$sth->execute(mktime(0,0,0,(localtime)[3,4,5]), time());
	
	my %stat = map { $_ => {count => 0, duration => 0} } values %$phones;
	while (my ($operator, $duration) = $sth->fetchrow_array()) {
		$stat{$phones->{$operator}}{count}++;
		$stat{$phones->{$operator}}{duration} += $duration;
	}
	
	$self->render(json => \%stat);
};

post '/exit' => sub {
	my $self = shift;
	
	$self->signed_cookie('login' => '',  {expires => time-360*24*60*60});
	$self->signed_cookie('passw' => '',  {expires => time-360*24*60*60});
	
	$self->redirect_to('/');
};

app->secrets([SECRET]);
app->start();

sub get_phones {
	my %phones;
	
	open my $fh, '<:utf8', HOME . '/' . PHONE_CFG or return {};
	while (my $str = <$fh>) {
		$str =~ s/\s+$//;
		my ($num, $operator) = split /\s*=\s*/, $str;
		$phones{$num} = $operator;
	}
	close $fh;
	
	return \%phones;
}

sub get_emails {
	my @emails;
	open my $fh, '<:utf8', HOME . '/' . MAIL_CFG or return [];
	while (my $str = <$fh>) {
		$str =~ s/\s+$//;
		push @emails, $str;
	}
	close $fh;
	
	return \@emails;
}

__DATA__
@@ layouts/root.html.ep
<!DOCTYPE html>
<html lang="ru">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta http-equiv="Pragma" content="no-cache" />
		<title><%= title %></title>
		%= javascript 'https://code.jquery.com/jquery-1.11.1.min.js';
		%= stylesheet '//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css';
		%= stylesheet '/style.css';
	</head>
	<body>
		%= content
	</body>
</html>

@@ auth.html.ep
% layout 'root', title => 'Телепродажи / Авторизация';
<div class="container" id="login-container">
	<div style="position:absolute; top:40%">
		<div class="content">
			<form method='post' action='/login'>
				<fieldset style="text-align:center" class="control-group">
					<div><input type="text" name="login" placeholder="Логин" class="input-centered"></div>
					<div><input type="password" name="passw" placeholder="Пароль" class="input-centered"></div>
					<button class="btn btn-primary" type="submit">Войти</button>
				</fieldset>
			</form>
		</div>
		
		% if (stash 'error') {
			<div class="error">неверный логин или пароль</div>
		% }
	</div>
</div>

@@ adm.html.ep
% layout 'root', title => 'Телепродажи / Управление';
<script>
	function time2hms(time) {
		var hours = parseInt(time/60/60);
		if (hours < 10) hours = '0'+hours;
		
		var minutes = parseInt((time - hours*60*60) / 60);
		if (minutes < 10) minutes = '0'+minutes;
		
		var seconds = parseInt(time - hours*60*60 - minutes*60);
		if (seconds < 10) seconds = '0'+seconds;

		return hours + ':' + minutes + ':' + seconds;
	}

	function load_stat(expl) {
		var stat_box = $('#stat');
		stat_box.text('Загрузка...');
		
		$.ajax({
			dataType: "json",
			url: "/stat",
			success: function(data, textStatus, jqXHR) {
				console.log(data);
				var table = $('<table class="table"></table>');
				table.append($('<tr><th>Оператор</th><th>Количество звонков (&gt;= <%= $ok_duration %> сек.)</th><th>Длительность</th></tr>'));
				
				var keys = [];
				
				for (k in data) {
					if (data.hasOwnProperty(k)) {
						keys.push(k);
					}
				}

				keys.sort();

				for (i = 0; i < keys.length; i++) {
					var tr = $('<tr></tr>');
					var td = $('<td></td>');
					td.text(keys[i]);
					tr.append(td);
					
					td = $('<td></td>');
					td.text(data[keys[i]]["count"]);
					tr.append(td);
					
					td = $('<td></td>');
					td.text(time2hms(data[keys[i]]["duration"]));
					tr.append(td);
					
					table.append(tr);
				}
				
				stat_box.html(table);
				
				% if (my $to = param 'to') {
					if (!expl) document.location.href = "#<%= $to %>"
				% }
			},
			error: function(jqXHR, textStatus, errorThrown) {
				stat_box.html('<span class="error">Ошибка получения статистики</span>');
			}
		});
	}
	
	$(function() {
		load_stat();
	});
</script>
<div class="container">
	<form method='POST' action='/exit' style="text-align: right">
		<button type="submit" class="btn btn-danger">Выход</button>
	</form>
	<h2>Статистика по операторам <button type="button" class="btn btn-default btn-xs" onclick="load_stat(true)">обновить <span class="glyphicon glyphicon-refresh"></span></button></h2>
	<div id="stat"></div>
	
	<h2>Список операторов</h2>
	<form method='POST' action='/addphone'>
		<table class="norm-table">
			<tr>
				<th>Оператор</th><td><input type="text" name="operator"></td>
			</tr>
			<tr>
				<th>Номер</th><td><input type="text" name="num"></td>
			</tr>
		</table>
		<button type="submit" class="btn btn-info btn-sm">добавить</button>
		<a name="phone">&nbsp;</a>
		% if (my $err = flash 'phone_error') {
			<span class="error"> <%= $err %> </span>
		% }
	</form>
	
		
	<br><br>
	<table class="table">
		<tr>
			<th>Оператор</th><th>Номер</th><th>Удалить</th>
		</tr>
		% for my $num (sort { $phones->{$a} cmp $phones->{$b} } keys %$phones) {
			<tr>
				<td><%= $phones->{$num} %></td>
				<td><%= $num %></td>
				<td>
					<form method="POST" action="/rmphone">
						<button value="<%= $num %>" name="num" type="submit" class="btn btn-default btn-xs"><span class="glyphicon glyphicon-trash"></span></button>
					</form>
				</td>
			</tr>
		% }
	</table>
	
	<h2>Участники рассылки</h2>
	<form method='POST' action='/addemail'>
		<table class="norm-table">
			<tr>
				<th>E-mail</th><td><input type="text" name="email"></td>
			</tr>
		</table>
		<button type="submit" class="btn btn-info btn-sm">добавить</button>
		<a name="email">&nbsp;</a>
		% if (my $err = flash 'email_error') {
			<span class="error"> <%= $err %> </span>
		% }
	</form>
	
	<br><br>
	<table class="table">
		% for my $m (sort @$emails) {
			<tr>
				<td><%= $m %></td>
				<td>
					<form method="POST" action="/rmemail">
						<button value="<%= $m %>" name="email" type="submit" class="btn btn-default btn-xs"><span class="glyphicon glyphicon-trash"></span></button>
					</form>
				</td>
			</tr>
		% }
	</table>
</div>

@@ style.css
#login-container.container {
	width: 300px;
}

.container .content {
	background-color: #e4ecf6;
	padding: 20px 20px 5px 20px;
	-webkit-border-radius: 10px 10px 10px 10px;
	-moz-border-radius: 10px 10px 10px 10px;
	border-radius: 10px 10px 10px 10px;
	-webkit-box-shadow: 0 1px 2px rgba(0,0,0,.15);
	-moz-box-shadow: 0 1px 2px rgba(0,0,0,.15);
	box-shadow: 0 1px 2px rgba(0,0,0,.15);
}

.input-centered {
	text-align:center;
}

.input-centered::-webkit-input-placeholder {
	text-align:center;
}

.error {
	text-align: center;
	color: red;
}

.norm-table {
	border-collapse: separate;
	border-spacing: 3px;
}
