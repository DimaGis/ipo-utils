use strict;
use utf8;
use Getopt::Std;
use POSIX;
use DBI;
use MIME::Lite;
use Excel::Writer::XLSX;

use constant {
	DB_NAME         => 'statistic',
	DB_HOST         => '10.54.16.43',
	DB_USER         => 'ipouser',
	DB_PASS         => 'ilove2gisIpoU$er',
	MAIL_HOST       => 'mx.2gis.local',
	MAIL_FROM       => 'ipo@2gis.ru',
	STAT_DIR        => 'stat',
};

getopts('m:p:d:b:o:n:', \my %OPTS);
defined $OPTS{m} && defined $OPTS{p} && defined $OPTS{o} && defined $OPTS{n} && $OPTS{b} ~~ ['day', 'week', 'month', 'spechours']
	or die "usage: $0 -m mailcfg -p phonescfg -o ok_duration -n name_prefix -b day|week|month|spechours [-d dd.mm.yyyy]";

utf8::decode($OPTS{n});

if (exists $OPTS{d}) {
	my ($mday, $month, $year) = $OPTS{d} =~ /(\d{1,2})\.(\d{1,2})\.(\d{4})/
		or die "incorrect format for -d";
	
	$OPTS{d} = POSIX::mktime(0, 0, 0, $mday, $month - 1, $year - 1900);
}

my ($prev_tstmp_begin, $prev_tstmp_end, $subject, $stat_file_path) = info_by_behaviour($OPTS{b});

if (-e $stat_file_path) {
	exit;
}

my $fh;
my @emails;
open $fh, '<:utf8', $OPTS{m} or die $!;
while (my $str = <$fh>) {
	$str =~ s/\s+$//;
	push @emails, $str;
}
close $fh;
die "empty emails set" unless @emails;

my %phones;
open $fh, '<:utf8', $OPTS{p} or die $!;
while (my $str = <$fh>) {
	$str =~ s/\s+$//;
	my ($num, $sn) = split /\s*=\s*/, $str;
	$phones{$num} = $sn;
}
close $fh;
die "empty phones set" unless %phones;

my $dbh = DBI->connect(
	"dbi:mysql:database=" . DB_NAME . ';host=' . DB_HOST . ';mysql_connect_timeout=10',
	DB_USER,
	DB_PASS,
	{RaiseError => 1, mysql_enable_utf8 => 1}
);

my $sth = $dbh->prepare(
	"SELECT callingPartyNumber, duration, originalCalledPartyNumber, dateTimeOrigination
	 FROM cdr_statistic WHERE dateTimeOrigination between ? AND ? AND originalCalledPartyNumber<>''
	 AND substring(originalCalledPartyNumber, 1, 1)<>'b' AND callingPartyNumber IN (".join(',', map { $dbh->quote($_) } keys %phones).")"
);
$sth->execute($prev_tstmp_begin, $prev_tstmp_end);

my %statistic;
while (my $row = $sth->fetchrow_hashref()) {
	push @{$statistic{ $row->{callingPartyNumber} }}, {
		duration  => $row->{duration},
		number    => $row->{originalCalledPartyNumber},
		orig_hour => (localtime($row->{dateTimeOrigination}))[2]
	};
}
exit unless %statistic;

$sth->finish();
$dbh->disconnect();

my $xlsx_book  = Excel::Writer::XLSX->new($stat_file_path);
my $xlsx_sheet = $xlsx_book->add_worksheet();
my $header_fmt = $xlsx_book->add_format(bold => 1);
my $cell_fmt   = $xlsx_book->add_format(align => 'left');
$xlsx_sheet->set_column(0, 0, 25);
$xlsx_sheet->set_column(1, 1, 7);
$xlsx_sheet->set_column(2, 3, 25);
$xlsx_sheet->set_column(4, 4, 14);
$xlsx_sheet->set_column(5, 6, 33);

my $i = 0;

$xlsx_sheet->write(0, $i++, "Оператор", $header_fmt);
$xlsx_sheet->write(0, $i++, "Номер", $header_fmt);
$xlsx_sheet->write(0, $i++, "Кол-во результативных ($OPTS{o} сек.)", $header_fmt);
$xlsx_sheet->write(0, $i++, "Кол-во нерезультативных", $header_fmt);
$xlsx_sheet->write(0, $i++, "Кол-во недозвонов", $header_fmt);
$xlsx_sheet->write(0, $i++, "Всего звонков", $header_fmt);
$xlsx_sheet->write(0, $i++, "Всего уникальных звонков", $header_fmt);
$xlsx_sheet->write(0, $i++, "Общая длительность разговоров", $header_fmt);
$xlsx_sheet->write(0, $i++, "Средняя длительность разговора", $header_fmt);

my $row = 0;
my %hourly_statistic;

while (my ($op, $op_stat) = each %statistic) {
	my $ok_cnt;
	my $bad_cnt;
	my $na_cnt;
	my $total_duration;
	my %uniq_numbers;
	
	for my $s (@$op_stat) {
		$hourly_statistic{ $s->{orig_hour} }{total_duration} += $s->{duration};
		$total_duration += $s->{duration};
		
		$hourly_statistic{ $s->{orig_hour} }{uniq_numbers}{ $s->{number} } = 1;
		$uniq_numbers{$s->{number}} = 1;
		
		if ($s->{duration} == 0) {
			$hourly_statistic{ $s->{orig_hour} }{na_cnt}++;
			$na_cnt++;
		}
		elsif ($s->{duration} >= $OPTS{o}) {
			$hourly_statistic{ $s->{orig_hour} }{ok_cnt}++;
			$ok_cnt++;
		}
		else {
			$hourly_statistic{ $s->{orig_hour} }{bad_cnt}++;
			$bad_cnt++;
		}
	}
	
	my $total_cnt = $ok_cnt + $bad_cnt + $na_cnt;
	my $avg_duration = $ok_cnt ? time2hms($total_duration / $ok_cnt) : 0;
	$total_duration = time2hms($total_duration);
	
	$row++;
	$i = 0;
	
	$xlsx_sheet->write($row, $i++, $phones{$op}, $cell_fmt);
	$xlsx_sheet->write($row, $i++, $op, $cell_fmt);
	$xlsx_sheet->write($row, $i++, $ok_cnt, $cell_fmt);
	$xlsx_sheet->write($row, $i++, $bad_cnt, $cell_fmt);
	$xlsx_sheet->write($row, $i++, $na_cnt, $cell_fmt);
	$xlsx_sheet->write($row, $i++, $total_cnt, $cell_fmt);
	$xlsx_sheet->write($row, $i++, scalar(keys %uniq_numbers), $cell_fmt);
	$xlsx_sheet->write($row, $i++, $total_duration, $cell_fmt);
	$xlsx_sheet->write($row, $i++, $avg_duration, $cell_fmt);
}

$row += 2;
my $header_row = $row;
my %sum;

for my $segm ([0..9], (map { [$_] } 10..18), [19..23]) {
	my $ok_cnt;
	my $bad_cnt;
	my $na_cnt;
	my $total_duration;
	my %uniq_numbers;
	
	for my $hour (@$segm) {
		next unless exists $hourly_statistic{$hour};
		
		$ok_cnt  += $hourly_statistic{$hour}{ok_cnt};
		$bad_cnt += $hourly_statistic{$hour}{bad_cnt};
		$na_cnt += $hourly_statistic{$hour}{na_cnt};
		$total_duration += $hourly_statistic{$hour}{total_duration};
		while (my ($k, $v) = each %{$hourly_statistic{$hour}{uniq_numbers}}) {
			$uniq_numbers{$k} = $v;
		}
	}
	
	my $total_cnt = $ok_cnt + $bad_cnt + $na_cnt;
	my $avg_duration = $ok_cnt ? time2hms($total_duration / $ok_cnt) : 0;
	
	$row++;
	$i = 0;
	
	$xlsx_sheet->write($row, $i++, sprintf('с %02d:00 до %02d:00', $segm->[0], $segm->[-1]+1), $cell_fmt);
	$xlsx_sheet->write($row, $i++, '', $cell_fmt);
	$xlsx_sheet->write($row, $i++, $ok_cnt, $cell_fmt);
	$xlsx_sheet->write($row, $i++, $bad_cnt, $cell_fmt);
	$xlsx_sheet->write($row, $i++, $na_cnt, $cell_fmt);
	$xlsx_sheet->write($row, $i++, $total_cnt, $cell_fmt);
	$xlsx_sheet->write($row, $i++, scalar(keys %uniq_numbers), $cell_fmt);
	$xlsx_sheet->write($row, $i++, time2hms($total_duration), $cell_fmt);
	$xlsx_sheet->write($row, $i++, $avg_duration, $cell_fmt);
	
	$sum{ok_cnt} += $ok_cnt;
	$sum{bad_cnt} += $bad_cnt;
	$sum{na_cnt} += $na_cnt;
	$sum{total_cnt} += $total_cnt;
	$sum{uniq_numbers} += scalar(keys %uniq_numbers);
	$sum{total_duration} += $total_duration;
}

$row = $header_row;
$i = 0;

$xlsx_sheet->write($row, $i++, "Часы", $header_fmt);
$xlsx_sheet->write($row, $i++, "", $header_fmt);
$xlsx_sheet->write($row, $i++, $sum{ok_cnt}, $header_fmt);
$xlsx_sheet->write($row, $i++, $sum{bad_cnt}, $header_fmt);
$xlsx_sheet->write($row, $i++, $sum{na_cnt}, $header_fmt);
$xlsx_sheet->write($row, $i++, $sum{total_cnt}, $header_fmt);
$xlsx_sheet->write($row, $i++, $sum{uniq_numbers}, $header_fmt);
$xlsx_sheet->write($row, $i++, time2hms($sum{total_duration}), $header_fmt);
$xlsx_sheet->write($row, $i++, "∑", $header_fmt);

$xlsx_book->close();

eval {
	my $msg = MIME::Lite->new(
		From     => MAIL_FROM,
		To       => join(', ', @emails),
		Subject  => $subject,
		Type     => 'multipart/mixed'
	);
	$msg->attach(Path => $stat_file_path, Disposition => 'attachment', Type => "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
	$msg->send('smtp', MAIL_HOST);
};
if ($@) {
	warn $@;
	unlink $stat_file_path;
}

sub info_by_behaviour {
	my $behaviour = shift;
	my @localtime = human_localtime(exists $OPTS{d} ? $OPTS{d} : time());
	
	my $spec_hour_begin = 11;
	my $spec_min_begin  = 0;
	my $spec_hour_end   = 12;
	my $spec_min_end    = 30;
	
	my %prev_delta_begin = (day => 24*60*60, week => ($localtime[6]+6)*24*60*60, month => $localtime[3]*24*60*60, spechours => 0);
	my ($prev_mday_begin, $prev_month_begin, $prev_year_begin) = (localtime((exists $OPTS{d} ? $OPTS{d} : time()) - $prev_delta_begin{$behaviour}))[3,4,5];
	
	my $prev_tstmp_begin = POSIX::mktime(0, $behaviour eq 'spechours' ? $spec_min_begin : 0, $behaviour eq 'spechours' ? $spec_hour_begin : 0, $behaviour eq 'month' ? 1 : $prev_mday_begin, $prev_month_begin, $prev_year_begin);
	
	my %prev_delta_end = (day => 23*60*60+59*60+59, week => 6*24*60*60+23*60*60+59*60+59, month => ($prev_mday_begin-1)*24*60*60+23*60*60+59*60+59, spechours => ($spec_hour_end-$spec_hour_begin)*60*60+($spec_min_end-$spec_min_begin)*60+59);
	my $prev_tstmp_end = $prev_tstmp_begin + $prev_delta_end{$behaviour};
	
	my ($subj, $fname);
	
	if ($behaviour eq 'day' || $behaviour eq 'spechours') {
		my @lt = human_localtime($prev_tstmp_begin);
		$fname = sprintf(
			"%s/$OPTS{n}_Call_Stat_%s-%02d.%02d.%02d.xlsx",
			STAT_DIR, ['Понедельник', 'Вторник', 'Среда', 'Четверг', 'Пятница', 'Суббота', 'Воскресенье']->[ $lt[6]-1 ], @lt[3, 4, 5]
		);
		$subj = sprintf("$OPTS{n} - Ежедневная статистика по операторам за %02d.%02d.%d", @lt[3, 4, 5]);
	}
	elsif ($behaviour eq 'week') {
		my @lt_begin = human_localtime($prev_tstmp_begin);
		my @lt_end   = human_localtime($prev_tstmp_end);
		
		$fname = sprintf(
			"%s/$OPTS{n}_Call_Stat_%02d.%02d.%d--%02d.%02d.%d.xlsx",
			STAT_DIR, @lt_begin[3,4,5], @lt_end[3,4,5]
		);
		$subj = sprintf(
			"$OPTS{n} - Еженедельная статистика по операторам за период %02d.%02d.%d - %02d.%02d.%d",
			@lt_begin[3,4,5], @lt_end[3,4,5]
		);
	}
	elsif ($behaviour eq 'month') {
		my @lt = localtime($prev_tstmp_begin);
		my @monthes = ('Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь');
		
		$fname = sprintf("%s/$OPTS{n}_Call_Stat_%s-%d.xlsx", STAT_DIR, $monthes[ $lt[4] ], $lt[5] + 1900);
		$subj = sprintf("$OPTS{n} - Ежемесячная статистика по операторам за %s %d года", $monthes[ $lt[4] ], $lt[5] + 1900);
	}
	
	if ($behaviour eq 'spechours') {
		my $spec_hours = sprintf('_%02d-%02d_%02d-%02d', $spec_hour_begin, $spec_min_begin, $spec_hour_end, $spec_min_end);
		$fname =~ s/\.xlsx$/$spec_hours.xlsx/;
		$subj .= sprintf(' %02d:%02d-%02d:%02d', $spec_hour_begin, $spec_min_begin, $spec_hour_end, $spec_min_end);
	}
	
	return ($prev_tstmp_begin, $prev_tstmp_end, $subj, $fname);
}

sub time2hms {
	my $time = $_[0];
	
	my $hours = int ($time/60/60);
	my $minutes = int (($time - $hours*60*60) / 60);
	my $seconds = int ($time - $hours*60*60 - $minutes*60);

	return sprintf('%02d:%02d:%02d', $hours, $minutes, $seconds);
}

sub human_localtime {
	my @lt = @_ ? localtime(shift) : localtime;
	$lt[6] = 7 if $lt[6] == 0;
	$lt[4] ++;
	$lt[5] += 1900;
	
	return @lt;
}
