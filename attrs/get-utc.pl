use strict;
use LWP::Simple;
use Text::CSV;
use URI::Escape;
use utf8;

binmode STDOUT, ':utf8';

my ($input, $output) = @ARGV;
$input && $output or die "usage: $0 input output";

my $reference = +7;

my $csv = Text::CSV->new({sep_char => ",", eol => "\n"});
my %utc;
if (-e $output) {
	open my $fh, '<:utf8', $output or die $!;
	while (my $row = $csv->getline($fh)) {
		$utc{$row->[0]} = $row->[1];
	}
}

open my $fh, '<:utf8', $input or die $!;
$LWP::Simple::ua->agent('Mozilla/5.0');

while (my $city = <$fh>) {
	$city =~ s/\s+$//;
	unless (exists $utc{$city}) {
		print "Fetching $city...\n";
		
		my $html = get(my $url = 'http://ru.wikipedia.org/w/index.php?search='.uri_escape_utf8($city).'&title=%D0%A1%D0%BB%D1%83%D0%B6%D0%B5%D0%B1%D0%BD%D0%B0%D1%8F%3A%D0%9F%D0%BE%D0%B8%D1%81%D0%BA');
		if (my ($utc) = $html =~ /title\s*=\s*"Часовой пояс".{10,300}?UTC([+-]\d+)/s) {
			$utc -= $reference;
			$utc{$city} = $utc;
		}
		else {
			$utc{$city} = undef;
		}
	}
}

open $fh, '>:utf8', $output or die $!;
while (my ($city, $delta) = each %utc) {
	$csv->print($fh, [$city, $delta]);
}
close $fh;
