use strict;
use utf8::all;
use LWP::Simple;
use URI::Escape;
use JSON;
use Text::CSV;
use composition ('LWP::UserAgent', 'LWP::UserAgent::Determined', 'LWP::UserAgent::Cached');

my $output = shift
	or die "usage: $0 output";

mkdir '/tmp/cache' || die "mkdir /tmp/cache: $!" unless (-e '/tmp/cache');
$LWP::Simple::ua = LWP::UserAgent::Cached->new(
	cache_dir  => '/tmp/cache',
	recache_if => sub {
		my ($resp, $path) = @_;
		return -M $path > 5 || $resp->code >= 400;
	}
);

print "Enter list of rubrics (one per line, Ctrl+D to continue):\n";
my @rubrics = map { trim($_ ) } <>;
unless (@rubrics) {
	die "Empty rubrics\n";
}
print "Validating input...\n";
my %orig_rubrics; @orig_rubrics{rubrics()} = ();
my @unknown_rubrics = grep { !exists $orig_rubrics{$_} } @rubrics;
if (@unknown_rubrics) {
	$" = "\n";
	die "Unknown rubrics:\n@unknown_rubrics\n";
}

print "Enter list of projects (one per line, Ctrl+D to continue, leave blank to process all):\n";
my @projects = map { trim($_) } <>;
print "Validating input...\n";
my %orig_projects; @orig_projects{projects()} = ();
my @unknown_projects = grep { !exists $orig_projects{$_} } @projects;
if (@unknown_projects) {
	$" = "\n";
	die "Unknown projects:\n@unknown_projects\n";
}
unless (@projects) {
	@projects = keys %orig_projects;
}

open my $fh, '>', $output or die "open $output: $!";
my $csv = Text::CSV->new({sep_char => ",", eol => "\n", binary => 1, always_quote => 1});

for my $project (@projects) {
	print "Fetching $project...\n";
	
	my %uid;
	for my $rubric (@rubrics) {
		print "\tFetching $rubric...\n";
		
		my $total;
		my $page = 1;
		
		# perl sux
		{do {
			my $info = decode_json(get('http://catalog.api.2gis.ru/searchinrubric?what='.uri_escape_utf8($rubric).'&where='.uri_escape_utf8($project).'&page='.$page.'&pagesize=30&sort=relevance&version=1.3&key=1'));
			if (! defined $total) {
				$total = $info->{total};
			}
			
			last unless exists $info->{result}; # buggy api
			
			for my $res (@{$info->{result}}) {
				$total--;
				next if $uid{$res->{id}}++;
				my $info = decode_json(get('http://catalog.api.2gis.ru/profile?id='.$res->{id}.'&key=1&version=1.3&lang=ru&limit=10'));
				
				my $uid = $res->{id};
				my $fid = $res->{firm_group}{id};
				my $name = $info->{name};
				my $addr = $info->{city_name} . ', ' . $info->{address};
				my @emails;
				my @phones;
				my @faxes;
				my $www;
				for my $cs (@{$info->{contacts}}) {
					for my $c (@{$cs->{contacts}}) {
						if ($c->{type} eq 'email') {
							push @emails, $c->{value};
						}
						elsif ($c->{type} eq 'phone') {
							push @phones, $c->{value};
						}
						elsif ($c->{type} eq 'fax') {
							push @faxes, $c->{value};
						}
						elsif ($c->{type} eq 'website') {
							$www = $c->{alias};
						}
					}
				}
				
				push @phones, @faxes;
				
				my $schedule;
				my $min_hours = 99;
				
				if (exists $info->{schedule}) {
					while (my ($week_day, $day_schdl) = each %{$info->{schedule}}) {
						next unless $week_day ~~ [qw/Mon Tue Wed Thu Fri Sat Sun/];
						
						my $from;
						my $to;
						
						for my $k (sort keys %$day_schdl) {
							unless (defined $from) {
								$from = $day_schdl->{$k}{from};
							}
							
							$to = $day_schdl->{$k}{to};
						}
						
						$from =~ s/:.+//;
						$to   =~ s/:.+//;
						
						if ($from < $min_hours) {
							$min_hours = $from;
							$schedule = "$from-$to";
						}
					}
				}
				
				unless (exists $info->{rubrics}) {
					$info->{rubrics} = [$rubric];
				}
				
				$csv->print($fh, [$project, '', $fid, $uid, $name, $addr, join('; ', grep { $_ } @phones[0,1,2]), $emails[0], join('; ', @{$info->{rubrics}}), $schedule, $www]);
			}
			
			$page++;
			
		} while ($total > 0);}
	}
}

close $fh;

sub projects {
	return map { $_->{name} } @{decode_json(get('http://catalog.api.2gis.ru/project/list?version=1.3&key=1'))->{result}};
}

sub rubrics {
	my @rubrics;
	
	my $first_level = decode_json(get('http://catalog.api.2gis.ru/rubricator?where=%D0%BC%D0%BE%D1%81%D0%BA%D0%B2%D0%B0&version=1.3&key=1'))->{result};
	for my $rubric (@$first_level) {
		my $second_level = decode_json(get('http://catalog.api.2gis.ru/rubricator?where=%D0%BC%D0%BE%D1%81%D0%BA%D0%B2%D0%B0&version=1.3&key=1&parent_id=' . $rubric->{id}))->{result};
		for my $rubric (@$second_level) {
			push @rubrics, $rubric->{name};
		}
	}
	
	return @rubrics;
}

sub trim {
	my $str = shift;
	$str =~ s/^\s+//;
	$str =~ s/\s+$//;
	$str;
}
