use strict;
use utf8;
use DBI;
use Text::CSV;

binmode STDOUT, ':utf8';
binmode STDERR, ':utf8';

my @INFO_RUSSIA = (
"Владивосток",
"Екатеринбург",
"Казань",
"Калининград",
"Москва",
"Находка",
"Нижний Новгород",
"Орёл",
"Пермь",
"Петрозаводск",
"Рязань",
"Самара",
"Санкт-Петербург",
"Саранск",
"Саратов",
"Тамбов",
"Тольятти",
"Тула",
"Уссурийск",
"Уфа"
);

my ($db_cfg_path, $input) = @ARGV;
$db_cfg_path && $input or die "usage: $0 db_cfg_path input";

sub read_config_file {
		my $fname = shift;
		my %retval;
		open my $fh, '<:utf8', $fname or return \%retval;
		while (my $str = <$fh>) {
				next if ($str =~ /^\s*#/);
				if (my ($k, $v) = $str =~ /(.+?)\s*=\s*+([^\r\n]+)/) {
						if (exists $retval{$k}) {
							if (ref $retval{$k}) {
								push @{$retval{$k}}, $v;
							}
							else {
								$retval{$k} = [$retval{$k}, $v];
							}
						}
						else {
							$retval{$k} = $v;
						}
				}
		}
		close $fh;

		return \%retval;
}

sub parse_db_string {
	my ($host, $port, $path) = $_[0] =~ m!([^/: ]+)\s*(?:/(\d+))?[: ](.+)! or return;
	$port = 3050 unless $port;
	
	return ($host, $port, $path);
}

open my $fh, '<:utf8', $input or die $!;
my $csv = Text::CSV->new({sep_char => ","});
my $header = $csv->getline($fh);
my ($city_column) = grep { lc $header->[$_] eq 'city' } 0..$#{$header};
my %citylist;
while (my $row = $csv->getline($fh)) {
	next if $row->[$city_column] ~~ @INFO_RUSSIA;
	$citylist{$row->[$city_column]} = 1;
}

my $cfg = read_config_file($db_cfg_path);
foreach my $city (keys %citylist) {
	unless (exists $cfg->{$city}) {
		die $city, " not found in the config";
	}
}

warn "I'll print bad cities (this may take a while):\n";

foreach my $city (keys %citylist) {
	my ($host, $port, $path) = parse_db_string($cfg->{$city});
	eval {
		my $dbh = DBI->connect("dbi:Firebird:db=$path;host=$host;ib_dialect=3;ib_role=ODBRUSER;ib_charset=UTF8", 'DGPP_USR', 'd7NwS3xB', {ib_enable_utf8 => 1, RaiseError => 1, PrintWarn => 1, PrintError => 1});
		$dbh->disconnect();
	};
	if ($@) {
		my ($e) = $@ =~ /(.+)/; # only first line
		warn "\e[1m$city\e[0m: $e\n";
	}
}
