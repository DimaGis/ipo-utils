use strict;
use utf8;
use Text::CSV;
use DBI;
use lib '/home/oleg/repos/orginfo/lib';
use App::OrgInfo;
use App::OrgInfo::DGPP;
use App::OrgInfo::Controller;

binmode STDOUT, ':utf8';

#"Бары,Кафе,Кафе-кондитерские / Кофейни,Кафе / рестораны быстрого питания,Пабы,Пиццерии,Рестораны,Рюмочные,Столовые,Суши-бары / рестораны,Караоке-клубы,Ночные клубы"
#"Автозаправочные станции (АЗС),Автогазозаправочные станции (АГЗС)"

my ($rbr, $temp_file) = (shift, shift);
$rbr && $temp_file or die "usage: $0 rbr temp_file [rbr_db]";

my $rbr_db = shift || $rbr;
utf8::decode($rbr);
$rbr = join ',', map { "'$_'" } split /,/, $rbr;


my $dbh = DBI->connect(
	"dbi:mysql:database=additional_attributes;host=10.54.17.30",
    'attruser',
    'TodoHLS',
    {RaiseError => 1, mysql_enable_utf8 => 1}
);

my $sth = $dbh->prepare("SELECT cityname, connstr FROM dbcfg WHERE inforussia=0");
$sth->execute();

my $sth_uid = $dbh->prepare(
	"SELECT distinct collection.uid FROM collection
	LEFT JOIN attributes on attr_id=attributes.id
	LEFT JOIN category on cat_id=category.id
	LEFT JOIN orgs on collection.uid=orgs.uid
	LEFT JOIN projects ON proj_id=projects.id
	WHERE projects.name=? AND category.name=?"
);

my $fh;
my $csv = Text::CSV->new({sep_char => ",", eol => "\n", binary => 1});
my %city_fetched;
if (-e $temp_file) {
	open $fh, '<:utf8', $temp_file or die $!;
	while (my $row = $csv->getline($fh)) {
		$city_fetched{$row->[0]} = 1;
	}
	close $fh;
	
	unless ($csv->eof) {
		my ($cde, $str, $pos) = $csv->error_diag ();
		die "FATAL!!! ", "($cde) $str: $pos\n";
	}
	
	
	open $fh, '>>:utf8', $temp_file or die $!;
}
else {
	open $fh, '>:utf8', $temp_file or die $!;
}

my $app        = App::OrgInfo->new();
my $controller = App::OrgInfo::Controller->new(app => $app);

while (my ($city, $dbpath) = $sth->fetchrow_array()) {
	next if exists $city_fetched{$city};
	
	print "Fetching $city...\n";
	
	my $dbh_fb = eval { App::OrgInfo::DGPP->new($controller, $city)->{dbh} };
	unless ($dbh_fb) {
		warn "Error: $@";
		next;
	}
	my $sth_fb = $dbh_fb->prepare(qq@execute block
			returns
			(
				O_FIRM_UID bigint,
				O_ADDR_UID bigint,
				O_FIRM_NAME varchar(250),
				O_ADDRESS varchar(1024),
				O_PHONES varchar(4096),
				O_RUBRICS varchar(4096)
			)
			as
				
			declare variable v_firm_ver integer;
			declare variable v_addr_id integer;
			declare variable v_phone varchar(20);
			declare variable v_phone_zone varchar(10);
			declare variable v_rubric varchar(100);


			begin
				
				for
					select distinct
						(((select tsite.uniqcode from t_csyssite tsite where tsite."DEFAULT" = 1) * cast(power(2.0,47) as bigint)) + (125 * cast(power(2.0, 32) as bigint)) + tf."_ID")
						, tf.name
						, tf."_VERSION"
					from t_testfirm tf
						inner join u_testfirm uf on tf."_VERSION" = uf."_VERSION"
						inner join t_testaddr ta on ta."_PID" = tf."_VERSION"
						inner join t_testarub trubs on trubs."_PID" = ta."_ID"
						inner join u_cestrubs urub on trubs.rub = urub."_ID"
						inner join t_cestrubs trub on trub."_VERSION" = urub."_VERSION"
					where tf.hide = 0
						and tf.hidetmp = 0
						and ta.hidetmp = 0
						and ta.hide = 0
						and trub.name in
						(
							$rbr
						)
					into
						 :O_FIRM_UID
						, O_FIRM_NAME
						, :v_firm_ver
				do
				begin
					
					for
						select distinct ( ((select tsite.uniqcode from t_csyssite tsite where tsite."DEFAULT" = 1) * cast(power(2.0,47) as bigint)) + (123 * cast(power(2.0, 32) as bigint)) + ta."_SID" ) as "o_addr_UID"
							, ta."_ID"
							, IIF(tstr."_ID" is null, '!Empty address!', IIF(ta.a1hadr is null, (tcity.name || ', ' || tstr.nul || ', ' || ta.a2house), (tcity.name || ', ' || tstr.nul || ', ' || thadr.number)))
						from t_testaddr ta
							left join t_testarub trubs on trubs."_PID" = ta."_ID"
							left join u_cestrubs urub on trubs.rub = urub."_ID"
							left join t_cestrubs trub on trub."_VERSION" = urub."_VERSION"
							left join t_umtbhadr thadr on ta.a1hadr = thadr."_ID"
							left join t_umtbstr0 tstr on thadr.street = tstr."_ID"
								or ta.a2street = tstr."_ID"
							left join t_umapcity tcity on tstr.city = tcity."_ID" or tcity."DEFAULT"=1
						where ta."_PID" = :v_firm_ver
							and  ta.hidetmp = 0
							and ta.hide = 0
							and (trub.name in
							(
								$rbr
							) or trub.name is null)
						order by tcity."DEFAULT"
						into :O_ADDR_UID
							, :v_addr_id
							, :O_ADDRESS
					do
					begin
						
						O_PHONES = '';

						for
							select distinct IIF(tpzon.code is null, '', tpzon.code)
								, tgpho.phone
							from t_testgpho tgpho
								inner join t_testagrp tagrp on tgpho."_PID" = tagrp."_ID"
								left join t_testczon tczon on tgpho.zone = tczon."_ID"
								left join t_cestpzon tpzon on tczon.code = tpzon."_ID"
							where tagrp."_PID" = :v_addr_id
								and tgpho.hide = 0
								and tgpho.hidetmp = 0
								and tgpho.isfax = 0
							--  and tgpho.revise = 0
							--	and not
							--	(
							--		upper(tgpho.comment) like upper('%справочная%')
							--		or (upper(tgpho.comment) like upper('%горячая линия%'))
							--	--    or (upper(tgpho.comment) like upper('%фраза 3%'))
							--	--    or (upper(tgpho.comment) like upper('%фраза 4%'))
							--	--    or (upper(tgpho.comment) like upper('%фраза 5%'))
							--	)
							into :v_phone_zone
								, :v_phone

						do
						begin
							if (O_PHONES = '') then
								O_PHONES = :v_phone_zone || ', ' || :v_phone;
							else
								O_PHONES = :O_PHONES || '; ' || :v_phone_zone || ', ' || :v_phone;
						end
						
						O_RUBRICS = '';
						
						for 
							select trub.name from t_testarub trubs
								inner join u_cestrubs urub on trubs.rub = urub."_ID"
								inner join t_cestrubs trub on trub."_VERSION" = urub."_VERSION"
							where trubs."_PID" = :v_addr_id
							into  :v_rubric
						do
						begin
							O_RUBRICS = O_RUBRICS || '; ' || :v_rubric;
						end
						--if ( "O_PHONES" != '') then
							suspend;
					end

				end

			end
	@);
	
	$sth_fb->execute();
	
	$sth_uid->execute($city, $rbr_db);
	my %inserted_uid;
	while (my ($uid) = $sth_uid->fetchrow_array()) {
		$inserted_uid{$uid} = 1;
	}
	
	while (my $row = $sth_fb->fetch()) {
		next if exists $inserted_uid{$row->[1]};
		$row->[-2] =~ s/,//g;
		$csv->print($fh, [$city, $rbr_db, @$row]);
		
		$inserted_uid{$row->[1]} = 1;
	}
	
	$sth_fb->finish();
	$dbh_fb->disconnect();
}

print "Success!\n";
