#include <windows.h>
#include <stdio.h>

int main(void) {
        HWND mainwnd = FindWindowA(NULL,"X-Lite");

        if(!mainwnd) {
                return 1;
        }

        PostMessage(mainwnd,WM_KEYDOWN, VK_ESCAPE, 0);
        PostMessage(mainwnd,WM_KEYUP, VK_ESCAPE, 0);

        return 0;
}
