use strict;
use utf8;
use DBI;
use Excel::Writer::XLSX;
use Parse::JCONF;
use File::Basename;
use Getopt::Std;
use POSIX;
use MIME::Lite;
use List::Util 'sum';
use IPOd::Client::Mail;
no if $^V >= v5.18, warnings => 'experimental::smartmatch';

use constant CONFIG => basename(__FILE__, '.pl').'.cfg';

getopts('b:d:c', \my %OPTS);

if ($OPTS{c}) {
	if (-e CONFIG) {
		die CONFIG, ' already exists';
	}
	
	open my $fh, '>:utf8', CONFIG or die CONFIG, ': ', $!;
	print $fh $_ while <DATA>;
	exit;
}

$OPTS{b} ~~ ['week', 'month']
	or die "usage:\n".
	       "\t$0 -b week|month [-d dd.mm.yyyy]\n".
	       "\t$0 -c\n";

my $cfg = Parse::JCONF->new(autodie => 1)->parse_file(CONFIG);
$SIG{__DIE__} = sub {
	if (defined($^S) && !$^S) {
		# not inside eval
		IPOd::Client::Mail->new(@{$cfg->{ipod}}{qw/host port user pass/})->send($cfg->{admin}, __FILE__. ' died at '.localtime, $_[0]);
	}
};

my %dst2city;
my %src2city;
while (my ($city, $v) = each %{$cfg->{cities}}) {
	for (@{$v->{dst}}) {
		die "Non unique dst `$_'" if exists $dst2city{$_};
		$dst2city{$_} = $city;
	}
	
	for (@{$v->{src}}) {
		die "Non unique src `$_'" if exists $src2city{$_};
		$src2city{$_} = $city;
	}
}

my %e8800;
@e8800{@{$cfg->{8800}}} = ();

my $dbh = DBI->connect(
	"dbi:mysql:database=" . $cfg->{db}{name} . ';host=' . $cfg->{db}{host} . ';mysql_connect_timeout=10',
	$cfg->{db}{user},
	$cfg->{db}{pass},
	{RaiseError => 1, mysql_enable_utf8 => 1, PrintError => 0}
);

my ($start, $end, $subj, $path) = info_by_behaviour($OPTS{b}, $cfg->{stat_dir}, $OPTS{d});

my $sth = $dbh->prepare(
	'SELECT callingPartyNumber, originalCalledPartyNumber, origDeviceName
	 FROM cdr_statistic
	 WHERE dateTimeOrigination between ? AND ?
	       AND originalCalledPartyNumber IN ('.gen_placeholders(scalar keys %dst2city).')
	       AND origDeviceName IN ('.gen_placeholders(scalar(keys %src2city) + @{$cfg->{8800}}).')'
);
$sth->execute($start, $end, keys %dst2city, keys %src2city, @{$cfg->{8800}});

my ($xlsx_book, $xlsx_sheet, $xlsx_header_fmt) = create_xlsx($path);
$xlsx_sheet->write(0, 0, 'Город', $xlsx_header_fmt);
$xlsx_sheet->write(0, 1, 'На городской номер филиала', $xlsx_header_fmt);
$xlsx_sheet->write(0, 2, 'В контакт-центр (далее звонок переведен в филиал)', $xlsx_header_fmt);
$xlsx_sheet->write(0, 3, 'Напрямую ушел в филиал с номера 8-800', $xlsx_header_fmt);
$xlsx_sheet->write(0, 4, 'Итого звонков по рекламе', $xlsx_header_fmt);

my %city;
while (my $row = $sth->fetchrow_hashref()) {
	my $city = $dst2city{$row->{originalCalledPartyNumber}};
	
	if (exists $src2city{$row->{origDeviceName}}) {
		if ($src2city{$row->{origDeviceName}} ne $city) {
			die "$src2city{$row->{origDeviceName}} != $city for $row->{origDeviceName}";
		}
		
		$city{$city}{local}++;
	}
	elsif (exists $e8800{$row->{origDeviceName}}) {
		if ($row->{callingPartyNumber} =~ /^\+\d{11}$/) {
			$city{$city}{8800}++
		}
		else {
			$city{$city}{office}++
		}
	}
	else {
		die "Unknown source: `$row->{origDeviceName}'";
	}
}

my $ri = 1;
for my $city (sort keys %{$cfg->{cities}}) {
	$xlsx_sheet->write($ri, 0, $city);
	$xlsx_sheet->write($ri, 1, $city{$city}{local}||0);
	$xlsx_sheet->write($ri, 2, $city{$city}{office}||0);
	$xlsx_sheet->write($ri, 3, $city{$city}{8800}||0);
	$xlsx_sheet->write($ri, 4, $city{$city}{local}+$city{$city}{office}+$city{$city}{8800});
	$ri++;
}

$xlsx_book->close();

my $msg = MIME::Lite->new(
	From    => $cfg->{mail_from},
	To      => join(', ', @{$cfg->{mailto}}),
	Subject => $subj,
	Type    => 'multipart/mixed'
);

$msg->attach(Path => $path, Disposition => 'attachment', Type => "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");

my $ipod = IPOd::Client::Mail->new(@{$cfg->{ipod}}{qw/host port user pass/});
$msg->send('smtp', $ipod->get_mail_host);

sub create_xlsx {
	my $path = shift;
	
	my $xlsx_book  = Excel::Writer::XLSX->new($path);
	my $xlsx_sheet = $xlsx_book->add_worksheet();
	my $xlsx_header_fmt = $xlsx_book->add_format(bold => 1);
	
	return ($xlsx_book, $xlsx_sheet, $xlsx_header_fmt);
}

sub info_by_behaviour {
	my ($behaviour, $stat_dir, $date) = @_;
	
	my $timestamp = time;
	if ($date) {
		my ($d, $m, $y) = $date =~ /^(\d{1,2})\.(\d{1,2})\.(\d{4})$/
			or die 'regexp failed';
		$timestamp = POSIX::mktime(0, 0, 0, $d, $m-1, $y-1900);
	}
	my @localtime = human_localtime($timestamp);
	
	my %prev_delta_begin = (week => ($localtime[6]+6)*24*60*60, month => $localtime[3]*24*60*60);
	my ($prev_mday_begin, $prev_month_begin, $prev_year_begin) = (localtime($timestamp - $prev_delta_begin{$behaviour}))[3,4,5];
	
	my $prev_tstmp_begin = POSIX::mktime(0, 0, 0, $behaviour eq 'month' ? 1 : $prev_mday_begin, $prev_month_begin, $prev_year_begin);
	
	my %prev_delta_end = (week => 6*24*60*60+23*60*60+59*60+59, month => ($prev_mday_begin-1)*24*60*60+23*60*60+59*60+59);
	my $prev_tstmp_end = $prev_tstmp_begin + $prev_delta_end{$behaviour};
	
	my ($subj, $fname);
	
	if ($behaviour eq 'week') {
		my @lt_begin = human_localtime($prev_tstmp_begin);
		my @lt_end   = human_localtime($prev_tstmp_end);
		
		$fname = sprintf(
			"%s/Incoming_Call_Stat_%02d.%02d.%d--%02d.%02d.%d.xlsx",
			$stat_dir, @lt_begin[3,4,5], @lt_end[3,4,5]
		);
		$subj = sprintf(
			"Еженедельная статистика по входящим звонкам за период %02d.%02d.%d - %02d.%02d.%d",
			@lt_begin[3,4,5], @lt_end[3,4,5]
		);
	}
	elsif ($behaviour eq 'month') {
		my @lt = localtime($prev_tstmp_begin);
		my @monthes = ('Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь');
		
		$fname = sprintf("%s/Incoming_Call_Stat_%s-%d.xlsx", $stat_dir, $monthes[ $lt[4] ], $lt[5] + 1900);
		$subj = sprintf("Ежемесячная статистика по входящим звонкам за %s %d года", $monthes[ $lt[4] ], $lt[5] + 1900);
	}
	
	return ($prev_tstmp_begin, $prev_tstmp_end, $subj, $fname);
}

sub human_localtime {
	my @lt = @_ ? localtime(shift) : localtime;
	$lt[6] = 7 if $lt[6] == 0;
	$lt[4] ++;
	$lt[5] += 1900;
	
	return @lt;
}

sub gen_placeholders {
	my $cnt = shift;
	return join(',', ('?')x$cnt);
}

__DATA__
stat_dir = "./stat"

db = {
	host: "localhost",
	user: "root",
	pass: "123",
	name: "foo"
}

cities = {
	"Новосибирск": {dst: ["*05404"], src: ["UK-VOICE-GW", "ACD-VOICE-GW"]},
}

8800 = ["UK-Ast01-FAX"]

mailto = [
	"o.gavrin@2gis.ru",
	"root@2gis.ru"
]

admin = "o.gavrin@gis.ru"

mail_from = "ipo@2gis.ru"

ipod = {
	host: "localhost",
	port: 17777,
	user: "root",
	pass: 123
}

web_auth = {
	login: "root",
	pass: "bar"
}
