use utf8;
use Mojolicious::Lite;
use Mojo::Home;
use Mojo::Util ('trim', 'md5_sum');
use Parse::JCONF;
use JCONF::Writer;
use Fcntl ':flock';
use constant CONFIG => Mojo::Home->new->detect(__PACKAGE__) . '/incoming-call-stat.cfg';

my $cfg = Parse::JCONF->new(autodie => 1)->parse_file(CONFIG);

post '/login' => sub {
	my $self = shift;
	
	if ($self->param('login') eq $cfg->{web_auth}{login} && $self->param('passw') eq $cfg->{web_auth}{pass}) {
		$self->signed_cookie('login' => $self->param('login'),  {expires => time+360*24*60*60});
		$self->signed_cookie('passw' => md5_sum($self->param('passw')),  {expires => time+360*24*60*60});
		
		$self->redirect_to('/');
		return 1;
	}
	
	$self->render('auth', error => 1);
};

under sub {
	my $self = shift;
	
	if ($self->req->headers->user_agent =~ /\bmsie\b/i) {
		# we hate IE
		return $self->redirect_to('http://bit.ly/u1Er3y');
	}
	
	unless ($self->signed_cookie('login') eq $cfg->{web_auth}{login} && $self->signed_cookie('passw') eq md5_sum($cfg->{web_auth}{pass})) {
		$self->render('auth');
		return;
	}
	
	1;
};

post '/logout' => sub {
	my $self = shift;
	
	$self->signed_cookie('login' => '',  {expires => time-360*24*60*60});
	$self->signed_cookie('passw' => '',  {expires => time-360*24*60*60});
	
	$self->redirect_to('/');
};

get '/' => sub {
	my $self = shift;
	$self->render('adm', cities => $cfg->{cities}, e8800 => $cfg->{8800}, emails => $cfg->{mailto});
};

post '/save' => sub {
	my $self = shift;
	my $scfg = $self->req->json;
	
	my (%src, @bad_src);
	my (%dst, @bad_dst);
	
	my @e8800 = @{$scfg->{8800}};
	
	while (my (undef, $c) = each %{$scfg->{cities}}) {
		for (@{$c->{src}}, @e8800) {
			if (exists $src{$_}) {
				push @bad_src, $_;
				next;
			}
			
			$src{$_} = 1;
		}
		
		for (@{$c->{dst}}) {
			if (exists $dst{$_}) {
				push @bad_dst, $_;
				next;
			}
			
			$dst{$_} = 1;
		}
		
		@e8800 = ();
	}
	
	my $error;
	unless (@bad_src || @bad_dst) {
		eval {
			local $cfg->{mailto} = $scfg->{mailto};
			local $cfg->{8800} = $scfg->{8800};
			local $cfg->{cities} = $scfg->{cities};
			
			my $jconf = JCONF::Writer->new(autodie => 1)->from_hashref($cfg);
			open my $fh, '>:utf8', CONFIG or die 'open: ', $!, "\n";
			flock $fh, LOCK_EX or die 'flock: ', $!, "\n";
			print $fh $jconf or die 'print: ', $!, "\n";
			close $fh or die 'close: ', $!, "\n";
		};
		if ($@) {
			$error = $@;
		}
		else {
			$cfg->{mailto} = $scfg->{mailto};
			$cfg->{8800} = $scfg->{8800};
			$cfg->{cities} = $scfg->{cities};
		}
	}
	else {
		$error = join("\r\n", "обнаружены повторяющиеся значения:", @bad_src, @bad_dst);
	}
	
	unless ($error) {
		$self->flash(saved => 1);
	}
	
	$self->render(json => {error => $error});
};

app->secrets([';lkrweopriwpesdfhjkhrweor!']);
app->start();


__DATA__
@@ layouts/root.html.ep
<!DOCTYPE html>
<html lang="ru">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta http-equiv="Pragma" content="no-cache" />
		<title><%= title %></title>
		%= javascript 'https://code.jquery.com/jquery-1.11.1.min.js';
		%= javascript 'https://code.jquery.com/ui/1.11.4/jquery-ui.js';
		%= stylesheet '//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css';
		%= stylesheet '//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css';
		%= stylesheet '/style.css';
	</head>
	<body>
		%= content
	</body>
</html>

@@ auth.html.ep
% layout 'root', title => 'Статистика звонков / Авторизация';
<div class="container" id="login-container">
	<div style="position:absolute; top:40%">
		<div class="content">
			<form method='post' action='/login'>
				<fieldset style="text-align:center" class="control-group">
					<div><input type="text" name="login" placeholder="Логин" class="input-centered"></div>
					<div><input type="password" name="passw" placeholder="Пароль" class="input-centered"></div>
					<button class="btn btn-primary" type="submit">Войти</button>
				</fieldset>
			</form>
		</div>
		
		% if (stash 'error') {
			<div class="error">неверный логин или пароль</div>
		% }
	</div>
</div>

@@ adm.html.ep
% layout 'root', title => 'Статистика звонков / Управление';
<script>
function magicTable(tbl, cols, onNew) {
	var tr = $('<tr>');
	var td = $('<td>');
	
	td.addClass('clickable');
	td.addClass('tip');
	td.text('кликнуть чтобы редактировать');
	td.attr('colspan', cols);
	td.click(function() {
		td.removeClass('clickable');
		td.removeClass('tip');
		td.text('');
		td.removeAttr('colspan');
		td.unbind('click');
		for (var i=1; i<cols; i++) {
			tr.append('<td></td>');
		}
		
		magicTable(tbl, cols, onNew);
		showChanges()
		onNew(tr);
	})
	
	tr.append(td);
	var tbody = tbl.children('tbody');
	if (!tbody.size()) {
		tbody = tbl;
	}
	tbody.append(tr);
	
	return tbl;
}

function onDelete() {
	$(this).parent().parent().hide(300, function() {
		$(this).remove();
	});
	showChanges()
}

function getDelBtn() {
	var btn = $('<button class="btn btn-default btn-xs trash"><span class="glyphicon glyphicon-trash"></span></button>');
	btn.click(onDelete);
	return btn;
}

var hasChanges = false;
function showChanges() {
	if (hasChanges) return;
	hasChanges = true;
	
	$('#changes-dialog').slideDown(300);
	$('#save-changes').removeAttr('disabled');
	$('#cancel-changes').removeAttr('disabled');
}

function getValue(td) {
	var input = td.find('input');
	var value = $.trim(input.size() ? input.val() : td.text());
	return value;
}

$(function() {
	% if (flash 'saved') {
		$('#saved-panel').show().fadeOut(3000);
	% }
	
	$("#tabs").tabs();
	
	$('button.trash').click(onDelete);
	
	$('#cancel-changes').click(function() {
		if (confirm("Это действие приведёт к потере всех несохранённых изменений. Продолжить?")) {
			window.location.reload()
		}
	})
	$('#save-changes').click(function() {
		var rv = {cities: {}, 8800: [], mailto: []};
		$('#city>tbody>tr').each(function() {
			var tds = $(this).children('td');
			
			if (tds[0].hasAttribute('colspan')) {
				return;
			}
			
			var city = getValue($(tds[0]));
			if (city.length == 0) return;
			
			var dst = [];
			$(tds[1]).find('.mini-table td:nth-child(1)').each(function() {
				if (this.hasAttribute('colspan')) {
					return;
				}
				var d = getValue($(this));
				if (d.length) {
					dst.push(d);
				}
			})
			
			var src = [];
			$(tds[2]).find('.mini-table td:nth-child(1)').each(function() {
				if (this.hasAttribute('colspan')) {
					return;
				}
				var s = getValue($(this));
				if (s.length) {
					src.push(s);
				}
			})
			
			rv["cities"][city] = {src: src, dst: dst}
		})
		
		$('#8800 td:nth-child(1)').each(function() {
			if (this.hasAttribute('colspan')) {
				return;
			}
			var e = getValue($(this));
			if (e.length) {
				rv[8800].push(e);
			}
		})
		
		$('#email td:nth-child(1)').each(function() {
			if (this.hasAttribute('colspan')) {
				return;
			}
			var e = getValue($(this));
			if (e.length) {
				rv['mailto'].push(e);
			}
		})
		
		$('#cancel-changes').attr('disabled', 'true');
		$('#save-changes').attr('disabled', 'true');
		var doc_cursor  = document.body.style.cursor;
		var this_cursor = this.style.cursor;
		document.body.style.cursor = this.style.cursor = 'wait';
		var that = this;
		
		function onFinish() {
			document.body.style.cursor = doc_cursor;
			that.style.cursor = this_cursor;
			$('#cancel-changes').removeAttr('disabled');
			$('#save-changes').removeAttr('disabled');
		}
		
		$.ajax('/save', {
			"type": "POST",
			"dataType": "json",
			"contentType": "application/json",
			"data": JSON.stringify(rv),
			"error": function(jqXHR, textStatus, errorThrown) {
				onFinish();
				alert("Ошибка сервера: " + textStatus)
			},
			"success": function(data, textStatus, jqXHR) {
				if (data['error']) {
					onFinish();
					alert("Ошибка сохранения\r\n" + data['error'])
				}
				else {
					window.location.reload()
				}
			}
		})
	})
	
	var miniHandler = function (tr) {
		var td = tr.children();
		$(td[0]).append('<input type="text">').find('input').focus();
		$(td[1]).append(getDelBtn());
	}
	
	magicTable($('#city'), 4, function (tr) {
		var td = tr.children();
		$(td[0]).append('<input type="text">').find('input').focus();
		$(td[1]).append(magicTable($('<table class="table mini-table"></table>'), 2, miniHandler));
		$(td[2]).append(magicTable($('<table class="table mini-table"></table>'), 2, miniHandler));
		$(td[3]).css("text-align", "right").append(getDelBtn());
	}).find('table.mini-table').each(function() { magicTable($(this), 2, miniHandler) })
	
	magicTable($('#8800'), 2, function (tr) {
		var td = tr.children();
		$(td[0]).append('<input type="text">').find('input').focus();
		$(td[1]).append(getDelBtn());
	})
	magicTable($('#email'), 2, function (tr) {
		var td = tr.children();
		$(td[0]).append('<input type="text">').find('input').focus();
		$(td[1]).append(getDelBtn());
	})
});
</script>
<div style="position:fixed; top:0px;left:40%;display:none;z-index:10" class="panel panel-danger" id="changes-dialog">
	<div class="panel-heading">
		<h3 class="panel-title">Несохранённые изменения</h3>
	</div>
	<div class="panel-body">
		<button type="button" id="save-changes" class="btn btn-success">Сохранить</button>
		<button type="button" id="cancel-changes" class="btn btn-danger">Отменить</button>
	</div>
</div>
<div style="position:fixed; top:0px;left:40%;display:none;z-index:10" class="panel panel-success" id="saved-panel">
	<div class="panel-heading">
		<h3 class="panel-title">Сохранено</h3>
	</div>
	<div class="panel-body">
		Все настройки успешно сохранены!
	</div>
</div>
<div class="container">
	<div id="tabs">
		<form method='POST' action='/logout' style="text-align: right">
			<button type="submit" class="btn btn-danger">Выход</button>
		</form>
		<ul>
			<li><a href="#tabs-1">Города</a></li>
			<li><a href="#tabs-2">8800</a></li>
			<li><a href="#tabs-3">Получатели</a></li>
		</ul>
		<div id="tabs-1">
			<table class="table" id="city">
				<thead>
					<tr>
						<th>имя</th><th>номера</th><th>устройства</th><th>удалить</th>
					</tr>
				</thead>
				<tbody>
				% for my $city (sort keys %$cities) {
					<tr>
						<td><%= $city %></td>
						<td>
							<table class="table mini-table">
								% for my $dst (@{$cities->{$city}{dst}}) {
									<tr>
										<td><%= $dst %></td>
										<td>
											<button class="btn trash btn-default btn-xs"><span class="glyphicon glyphicon-trash"></span></button>
										</td>
									</tr>
								% }
							</table>
						</td>
						<td>
							<table class="table mini-table">
								% for my $src (@{$cities->{$city}{src}}) {
									<tr>
										<td><%= $src %></td>
										<td>
											<button class="btn trash btn-default btn-xs"><span class="glyphicon glyphicon-trash"></span></button>
										</td>
									</tr>
								% }
							</table>
						</td>
						<td style="text-align:right">
							<button class="btn trash btn-default btn-xs"><span class="glyphicon glyphicon-trash"></span></button>
						</td>
					</tr>
				% }
				</tbody>
			</table>
		</div>
		<div id="tabs-2">
			<table class="table" id="8800">
				<thead>
					<tr>
						<th>устройство</th><th>удалить</th>
					</tr>
				</thead>
				<tbody>
				% for my $e (@$e8800) {
					<tr>
						<td><%= $e %></td>
						<td>
							<button class="btn trash btn-default btn-xs"><span class="glyphicon glyphicon-trash"></span></button>
						</td>
					</tr>
				% }
				</tbody>
			</table>
		</div>
		<div id="tabs-3">
			<table class="table" id="email">
				<thead>
					<tr>
						<th>email</th>
						<th>удалить</th>
					</tr>
				</thead>
				<tbody>
				% for my $email (@$emails) {
					<tr>
						<td><%= $email %></td>
						<td>
							<button class="btn trash btn-default btn-xs"><span class="glyphicon glyphicon-trash"></span></button>
						</td>
					</tr>
				% }
				</tbody>
			</table>
		</div>
	</div>
</div>

@@ style.css
#login-container.container {
	width: 300px;
}

.container .content {
	background-color: #e4ecf6;
	padding: 20px 20px 5px 20px;
	-webkit-border-radius: 10px 10px 10px 10px;
	-moz-border-radius: 10px 10px 10px 10px;
	border-radius: 10px 10px 10px 10px;
	-webkit-box-shadow: 0 1px 2px rgba(0,0,0,.15);
	-moz-box-shadow: 0 1px 2px rgba(0,0,0,.15);
	box-shadow: 0 1px 2px rgba(0,0,0,.15);
}

.input-centered {
	text-align:center;
}

.input-centered::-webkit-input-placeholder {
	text-align:center;
}

.error {
	text-align: center;
	color: red;
}

.norm-table {
	border-collapse: separate;
	border-spacing: 3px;
}

.clickable {
	cursor: pointer;
}

.tip {
	color: gray;
	font-style: italic;
	font-size: 80%;
	text-align: center;
}

.mini-table {
	font-size: 10px;
	text-align: center;
}
