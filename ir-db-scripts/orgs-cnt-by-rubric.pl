use strict;
use Parse::JCONF;
use FindBin;
use Text::Trim;
use Text::CSV;
use lib 'lib';
use DB;

my $cfg = Parse::JCONF->new(autodie => 1)->parse_file($FindBin::Bin.'/db.cfg');
my $dbh = DB::get_dbh(@$cfg{qw/db_host db_name db_user db_pass/});

my $output = shift
	or die "usage: $0 output";

open my $fh, '>:utf8', $output
	or die $!;

my $sth_rubric = $dbh->prepare(
	'select rubric.id from rubric
	 left join rubric_names  on rubric.id=rubric_id
	 where rubric.hidden=0 and lang="ru" and rubric_names.name=?'
);

my %rubrics;
print "Enter rubric name, one per line (^D to quit):\n";
while (my $rubric = <STDIN>) {
	trim $rubric;
	$sth_rubric->execute($rubric);
	my ($id) = $sth_rubric->fetchrow_array()
		or die "Unknown rubric: ", $rubric;
	utf8::decode($rubric);
	$rubrics{$rubric} = $id;
}

print "Calculating...\n";

my $sth_branch = $dbh->prepare('select branch_id, name from branch_names where lang="ru"');
$sth_branch->execute();

my $sth_card = $dbh->prepare(
	'select count(card.id) from card
	 inner join card_rubrics on card_rubrics.card_id=card.id
	 where card.hidden=0 and card.archived=0 and card.local=1 and
	       card.type="POS" and card.branch_id=? and card_rubrics.rubric_id=?'
);

my @rubrics = keys %rubrics;
my $csv = Text::CSV->new({eol => "\n", binary => 1});

$csv->print($fh, ['', @rubrics]);

while (my ($id, $name) = $sth_branch->fetchrow_array()) {
	my @res = $name;
	for my $rubric_name (@rubrics) {
		$sth_card->execute($id, $rubrics{$rubric_name});
		my $cnt = $sth_card->fetch->[0];
		push @res, $cnt;
	}
	
	$csv->print($fh, \@res);
}

close $fh;
