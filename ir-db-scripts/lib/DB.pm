package DB;

use strict;
use DBI;

sub get_dbh {
	my ($host, $db, $user, $password) = @_;
	
	my $dbh = DBI->connect(
		"dbi:mysql:database=$db;host=$host;mysql_connect_timeout=10",
		$user,
		$password,
		{RaiseError => 1, mysql_enable_utf8 => 1, PrintError => 0}
	);
}

1;
