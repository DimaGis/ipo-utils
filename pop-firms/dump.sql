CREATE TABLE `brands` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `www` varchar(255) DEFAULT NULL,
  `brand` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `www_idx` (`www`),
  KEY `brand_idx` (`brand`)
) ENGINE=InnoDB AUTO_INCREMENT=1920 DEFAULT CHARSET=utf8;

CREATE TABLE `firms` (
  `firm_id` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`firm_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `flamp` (
  `card_id` bigint(20) unsigned NOT NULL,
  `firm_id` bigint(20) unsigned NOT NULL,
  `project_id` int(10) unsigned NOT NULL,
  `has_business_account` tinyint(4) NOT NULL DEFAULT '0',
  `reviews_count` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`card_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
