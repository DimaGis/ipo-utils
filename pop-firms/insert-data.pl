use strict;
use Parse::JCONF;
use Text::CSV;
use DBI;

my $input = shift
	or die "usage: $0 input";

my $cfg = Parse::JCONF->new(autodie => 1)->parse_file('webapp.cfg');
my $dbh = DBI->connect(
	"dbi:mysql:database=" . $cfg->{db}{name} . ';host=' . $cfg->{db}{host} . ';mysql_connect_timeout=10',
	$cfg->{db}{user},
	$cfg->{db}{pass},
	{RaiseError => 1, mysql_auto_reconnect => 1, mysql_enable_utf8 => 1}
);

my $sth = $dbh->prepare('insert into pop_firms (firm_id) values (?)');

open my $fh, "<:utf8", $input or die $!;
my $csv = Text::CSV->new({binary => 1, sep_char => ','});

my $header = $csv->getline($fh);
while (my $row = $csv->getline($fh)) {
	$sth->execute($row->[3]);
}

$csv->eof or die 'csv error at line ', $.;
